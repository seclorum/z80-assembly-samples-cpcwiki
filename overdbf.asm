;; &0000-&0188      &188 bytes (392 bytes)
;; &0800-&0988      &188 bytes (392 bytes)
;; &1000-&1188      &188 bytes (392 bytes)
;; &1800-&1988      &188 bytes (392 bytes)
;; &2000-&2188      &188 bytes (392 bytes)
;; &2800-&2988      &188 bytes (392 bytes)
;; &3000-&3188      &188 bytes (392 bytes)
;; &3800-&3988      &188 bytes (392 bytes)
;; &4508-&4800    &2f8 (760 bytes)
;; &4d08-&5000   &2f8 (760 bytes)
;; &5508-&5800   &2f8 (760 bytes)
;; &5d08-&6000   &2f8 (760 bytes)
;; &6508-&6800   &2f8 (760 bytes)
;; &6d08-&7000   &2f8 (760 bytes)
;; &7508-&7800   &2f8 (760 bytes)
;; &7d08-&8188 &480 (1152 bytes)

;; &8800-&8988      &188 bytes (392 bytes)
;; &9000-&9188      &188 bytes (392 bytes)
;; &9800-&9988      &188 bytes (392 bytes)
;; &a000-&a188      &188 bytes (392 bytes)
;; &a800-&a988      &188 bytes (392 bytes)
;; &b000-&b188      &188 bytes (392 bytes)
;; &b800-&b988      &188 bytes (392 bytes)
;; &c508-&c800 &2f8 (760 bytes)
;; &cd08-&d000 &2f8 (760 bytes)
;; &d508-&d800 &2f8 (760 bytes)
;; &dd08-&e000 &2f8 (760 bytes)
;; &e508-&e800 &2f8 (760 bytes)
;; &ed08-&f000 &2f8 (760 bytes)
;; &f508-&f800 &2f8 (760 bytes)
;; &fd08-&ffff &2f8 bytes

;; 20344 bytes free.

org &7e00

start:
di
ld sp,&ffff
ld hl,&c9fb
ld (&0038),hl
ei

ld hl,&188
ld d,&aa
call cls
ld hl,&8188
ld d,&55
call cls

ld hl,crtc_vals
call set_screen

flip equ &0cc4 XOR &2cc4

flipl equ flip AND 255
fliph equ flip/256

fl:
ld b,&f5
fl1:
in a,(c)
rra
jr nc,fl1

;; flip the screen
ld hl,(base)
ld bc,&bc0c
out (c),c
inc b
out (c),h
dec b
inc c
out (c),c
inc b
out (c),l

halt

halt

halt

ld a,(base+0)
xor flipl
ld (base+0),a
ld a,(base+1)
xor fliph
ld (base+1),a

jp fl


cls:
ld bc,32*8
cls1:
push de
push bc
push hl
ld (hl),d
ld e,l
ld d,h
inc de
ld bc,46*2-1
ldir


pop hl
call _cpc_NextLineInternal
pop bc
pop de
dec bc
ld a,b
or c
jr nz,cls1
ret



base:
defw &0cc4


set_screen:
ld de,&bcbe
xor a
ss1:
ld b,d
out (c),a
ld b,e
outi
inc a
cp 14
jr nz,ss1
ret


;; HL = address
_cpc_NextLineInternal:
;; go down next scan line
ld a,h
add a,8
ld h,a
;; check if we should go to next char line
and &38
ret nz

;; remove effect of last add
ld a,h
sub &8
ld h,a

;; add on amount to go to next char line
ld a,l
add a,46*2
ld l,a
ld a,h
adc a,&00
ld h,a

;; if we overflowed to next 16k the result will be 0
and &38
ret z

;; we didn't overflow adjust to go back into 1st 16k
ld a,h
sub &38
ld h,a

	ret


crtc_vals:
defb &03f, 46,49, &086, &026, 0, 32, 35,0,8-1,0,0,&0c,196

end start