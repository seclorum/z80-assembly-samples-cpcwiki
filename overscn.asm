;; This example shows a simple method for a overscan
;; screen. 
;;
;; This example will work on CPC, CPC+ and KC Compact.
;;
;; This example uses the 6845 CRTC, and shows how
;; 32k of pixel data can be displayed by the CRTC.
;; This example is compatible with CRTC type 2. (MC6845)
;;
;; the screen is 48x35 visible characters in dimension,
;; and each character is 8 scanlines tall.
;; the visible window is moved so that the display
;; will fill the entire monitor screen. 
;; The screen base address is initialised, so that
;; MA11=MA10=1, and the internal MA counter of the CRTC will 
;; change MA12 during the display, and cause the CRTC to display
;; data from a 32k range. The actual visible display uses approx
;; 24k of RAM. After this setup, the screen does not need to
;; re-initialised for the overscan to be maintained.
;;
;; the screen is setup for a PAL display.
;;
;; This example will compile with the MAXAM assembler
;; or the built-in assembler of WinAPE32.
;;
;; Kevin Thacker 2002,2010

;; the origin of this source code is not important
org &4000

;; initialise a display window of 48x35 characters
;; each character is 8 scanlines tall

;; set character height to 8 scanlines
ld bc,&bc09
out (c),c
ld bc,&bd00+7
out (c),c

;; set default horizontal total (set horizontal total to 64 CRTC characters
;; = 64 microseconds)
ld bc,&bc00
out (c),c
ld bc,&bd00+&3f
out (c),c

;; set default vertical total (set vertical total to 39 CRTC character-lines)
ld bc,&bc04
out (c),c
ld bc,&bd00+38
out (c),c

;; setup default horizontal and vertical sync widths
;; this is compatible with CRTC type 2 based on the width
;; and hsync position defined in this example
ld bc,&bc03
out (c),c
ld bc,&bd00+&89
out (c),c

;; setup default vertical adjust
ld bc,&bc05
out (c),c
ld bc,&bd00
out (c),c

;; setup default interlace & skew
ld bc,&bc08
out (c),c
ld bc,&bd00
out (c),c

;; set width of display window
;; this value is compatible with crtc type 2
ld bc,&bc01
out (c),c
ld bc,&bd00+48
out (c),c

;; set horizontal sync position; and therefore the
;; horizontal position of the display window
;; within the monitor display
;;
;; this value is compatible with crtc type 2
ld bc,&bc02
out (c),c
ld bc,&bd00+48
out (c),c

;; set height of display window
ld bc,&bc06
out (c),c
ld bc,&bd00+35
out (c),c

;; set vertical sync position; and therefore the
;; vertical position of the display window
;; within the monitor display
ld bc,&bc07
out (c),c
ld bc,&bd00+35
out (c),c

;; set display start
;; force MA11=MA10=1, so that the internal MA
;; counter will increment enough to change MA12.

;; the displayed data is &0000-&7fff
ld bc,&bc0c
out (c),c
ld bc,&bd00+&0c
out (c),c
ld bc,&bc0d
out (c),c
ld bc,&bd00+0
out (c),c

ret
