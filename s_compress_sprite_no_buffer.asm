;; Sprite Compression Routine
;; Compresses a sprite in a format that can be decompressed directly to the screen without using a buffer
;; by redbox - October 2013
;; http://www.cpcwiki.eu/index.php/User:Redbox

			org	&4000

			sprite			equ	&6000
			compressed_sprite	equ	&8000
			sprite_width		equ	48	; bytes
			sprite_height		equ	75	; lines
			control_code		equ	&80



			ld	ix,sprite
			ld	hl,compressed_sprite

			ld	b,sprite_height
loop:			push	bc				; store counter
			call	line_loop			; compress one line
			ld	a,0				; reset line counter
			ld	(counter),a
			pop	bc				; restore counter
			djnz	loop				; and loop

			ret



line_loop:		ld	a,(counter)			; have we reached the end of the boundary
			cp	sprite_width			; yes?
			ret	z				; then RETurn

			ld	a,(counter)			; check if we are within 3 bytes of boundary
			cp	sprite_width-3
			jr	z,no_repetition
			cp	sprite_width-2
			jr	z,no_repetition
			cp	sprite_width-1
			jr	z,no_repetition


			ld	a,(ix+1)			; else, is the next byte the same?
			cp	(ix+0)			
			jr	nz,no_repetition		; no, then carry on and insert byte
			ld	a,(ix+2)			; is the next byte the same?
			cp	(ix+0)			
			jr	nz,no_repetition		; no, then carry on and insert byte
			ld	a,(ix+3)			; is the next byte the same?
			cp	(ix+0)			
			jr	nz,no_repetition		; no, then carry on and insert byte

			ld	(hl),control_code		; else, we have a repetition of more than 3 bytes
			inc	hl				; so insert control code into queue
			ld	a,(ix+0)			; insert the byte to repeat
			ld	(hl),a
			inc	hl
			ld	a,&01				; and start the repetition counter at 1 because decompress
			ld	(hl),a				; routine would have to do one more repetition if it were 0

			ld	a,(counter)			; also move the boundary counter on by 1
			add	a,1				; as we have just put 1 byte into the repetition counter
			ld	(counter),a			; to start it off

check_next_byte:	ld	a,(counter)			; have we reached the end of the boundary
			cp	sprite_width			; yes?
			jr	z,end_of_boundary		; then RETurn

			ld	a,(ix+1)			; is the next byte the same
			cp	(ix+0)			
			jr	z,increase_repetition		; yes, then increase the repetition counter

			inc	ix				; no, then we're finished so move onto the next byte
			inc	hl
			jr	line_loop			; and loop back



no_repetition:		ld	a,(ix+0)			; there's no repetition found
			ld	(hl),a				; so just store the byte
			inc	ix				; and move on to the next byte
			inc	hl
			ld	a,(counter)			; increase the width boundary counter by 1
			inc	a
			ld	(counter),a			; store it
			jr	line_loop			; and loop back	

increase_repetition:	inc	(hl)				; increase the repetition counter by 1
			inc	ix				; and move onto the next byte 
			ld	a,(counter)			; increase the width boundary counter by 1
			inc	a
			ld	(counter),a			; store it
			jr	check_next_byte			; and check next byte

end_of_boundary:	inc	ix
			inc	hl
			ret

counter:		defb	&00



			org	&6000

			defb &00,&00,&00,&00,&00,&00,&00,&00
			defb &00,&00,&00,&00,&00,&00,&11,&ee
			defb &00,&00,&00,&00,&00,&00,&00,&00
			defb &00,&00,&00,&00,&00,&ff,&ff,&ff
			defb &cc,&00,&00,&00,&00,&00,&00,&00
			defb &00,&00,&00,&00,&00,&00,&00,&00

			defb &00,&33,&ff,&ff,&88,&00,&00,&ff
			defb &ee,&00,&00,&00,&00,&00,&33,&ff
			defb &ff,&ff,&ff,&88,&00,&00,&00,&00
			defb &00,&00,&00,&00,&11,&ff,&ff,&ff
			defb &ff,&88,&00,&00,&00,&00,&00,&00
			defb &00,&00,&00,&11,&00,&00,&00,&00

			defb &00,&33,&ff,&ff,&ff,&cc,&00,&ff
			defb &ee,&00,&11,&ff,&ff,&cc,&33,&0f
			defb &7f,&ff,&ff,&cc,&00,&00,&00,&00
			defb &00,&00,&00,&11,&ff,&fe,&f0,&f0
			defb &f1,&88,&00,&77,&ff,&ff,&cc,&00
			defb &00,&00,&00,&33,&88,&cc,&00,&00

			defb &00,&77,&cf,&0f,&1f,&ff,&00,&cf
			defb &6e,&00,&11,&ff,&ff,&cc,&ff,&ff
			defb &0f,&0f,&0f,&cc,&00,&00,&00,&00
			defb &00,&00,&00,&33,&ff,&fe,&f0,&f0
			defb &f1,&88,&00,&77,&ff,&ff,&ff,&ff
			defb &88,&00,&00,&33,&ff,&ee,&00,&00

			defb &00,&ff,&ef,&0f,&0f,&1f,&00,&cf
			defb &6e,&00,&11,&8f,&0f,&dd,&ff,&ff
			defb &0f,&0f,&0f,&ee,&00,&11,&ff,&ff
			defb &ff,&ff,&dd,&ff,&f0,&f6,&f0,&f0
			defb &f1,&88,&77,&ff,&0f,&0f,&7f,&ff
			defb &88,&00,&00,&77,&f7,&e6,&00,&00

                        defb &77,&fe,&c7,&0f,&0f,&1f,&00,&cf
                        defb &6e,&00,&11,&8f,&0f,&dd,&f8,&f1
                        defb &8f,&0f,&0f,&ff,&00,&33,&ff,&ff
                        defb &ff,&ff,&ff,&ff,&f0,&f6,&f0,&f0
                        defb &f1,&88,&ff,&ff,&cf,&0f,&0f,&1f
                        defb &88,&00,&00,&76,&f3,&f3,&00,&00
                        
                        defb &76,&f0,&cf,&0f,&0f,&1f,&00,&cf
                        defb &6e,&00,&11,&8f,&0f,&dd,&f8,&f1
                        defb &8f,&0f,&0f,&7d,&88,&67,&0f,&0f
                        defb &0f,&0f,&0f,&ff,&f0,&f6,&f0,&f0
                        defb &f1,&88,&fe,&f0,&cf,&0f,&0f,&1f
                        defb &88,&00,&00,&fe,&f3,&f3,&00,&00
                        
                        defb &76,&f0,&cf,&0f,&0f,&3f,&cc,&cf
                        defb &6e,&00,&11,&8f,&0f,&dd,&f8,&f1
                        defb &8f,&0f,&0f,&7d,&88,&67,&0f,&0f
                        defb &0f,&0f,&0f,&ff,&f0,&f7,&ff,&ff
                        defb &ff,&88,&74,&f0,&cf,&0f,&0f,&1f
                        defb &cc,&00,&00,&fc,&f3,&f3,&88,&00
                        
                        defb &76,&f0,&ff,&ff,&ff,&3f,&cc,&cf
                        defb &6e,&00,&11,&8f,&0f,&dd,&f8,&f1
                        defb &ff,&ff,&ff,&fd,&88,&67,&0f,&0f
                        defb &0f,&0f,&0f,&ff,&f0,&f7,&ff,&ff
                        defb &ff,&88,&74,&f0,&cf,&0f,&0f,&1f
                        defb &ff,&00,&11,&fc,&f6,&f1,&88,&00
                        
                        defb &74,&f0,&ee,&ff,&ff,&ff,&cc,&cf
                        defb &6e,&00,&11,&8f,&0f,&dd,&f8,&f1
                        defb &00,&ff,&ff,&f8,&cc,&67,&0f,&0f
                        defb &0f,&0f,&0f,&ff,&f0,&e6,&00,&00
                        defb &00,&00,&74,&f0,&ff,&ff,&ff,&ff
                        defb &ff,&00,&11,&f8,&f6,&f1,&88,&00
                        
                        defb &fc,&f0,&88,&00,&76,&f0,&cc,&8f
                        defb &6e,&00,&00,&8f,&0f,&dd,&f8,&f1
                        defb &00,&00,&76,&f0,&cc,&67,&0f,&0f
                        defb &0f,&0f,&0f,&ff,&f0,&e6,&00,&00
                        defb &00,&00,&74,&f0,&cc,&77,&ff,&ff
                        defb &f3,&00,&33,&f0,&f6,&f0,&cc,&00
                        
                        defb &fc,&f0,&88,&00,&76,&f0,&cc,&8f
                        defb &6e,&00,&00,&8f,&0f,&dd,&f8,&f1
                        defb &00,&00,&76,&f0,&cc,&77,&ff,&ff
                        defb &cf,&3f,&ff,&dd,&f8,&e6,&00,&00
                        defb &00,&00,&74,&f0,&cc,&00,&11,&f8
                        defb &f3,&00,&33,&f0,&fe,&f0,&cc,&00
                        
                        defb &fc,&f1,&88,&00,&77,&f0,&cc,&8f
                        defb &6e,&00,&00,&8f,&0f,&dd,&f8,&f1
                        defb &00,&00,&33,&f0,&ee,&33,&ff,&ff
                        defb &ff,&ff,&cc,&11,&f8,&e6,&00,&ff
                        defb &cc,&00,&74,&f0,&cc,&00,&33,&f8
                        defb &e2,&00,&76,&f0,&fe,&f0,&ee,&00
                        
                        defb &fc,&f1,&cc,&00,&77,&ff,&dd,&8f
                        defb &6e,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &00,&00,&33,&f3,&ee,&00,&00,&76
                        defb &ff,&cc,&00,&11,&f8,&f7,&ff,&ff
                        defb &cc,&00,&74,&f0,&cc,&00,&33,&f0
                        defb &e2,&00,&76,&f0,&ff,&f0,&e6,&00
                        
                        defb &fc,&f1,&ff,&00,&11,&ff,&99,&8f
                        defb &6e,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &ff,&ff,&ff,&ff,&cc,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&f7,&cf,&0f
                        defb &6e,&00,&74,&f0,&cc,&00,&33,&f0
                        defb &e2,&00,&fc,&f1,&bb,&f0,&e6,&00
                        
                        defb &fc,&f1,&3f,&cc,&00,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &ff,&8f,&ef,&ee,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&dd,&ff,&ff,&f0
                        defb &e2,&11,&fc,&f1,&99,&f0,&f3,&00
                        
                        defb &fc,&f3,&0f,&ff,&88,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&ff,&ff,&ff,&f0
                        defb &e6,&11,&f8,&f1,&99,&f8,&f3,&00
                        
                        defb &ff,&ff,&0f,&1f,&ff,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&8f,&0f,&3f,&f0
                        defb &e6,&33,&f8,&f3,&11,&f8,&f3,&00
                        
                        defb &ff,&ef,&0f,&0f,&7f,&cc,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&8f,&0f,&3f,&fc
                        defb &e6,&33,&f0,&f3,&11,&f8,&f1,&88
                        
                        defb &00,&ff,&8f,&0f,&0f,&cc,&11,&8f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&ff,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&1f
                        defb &cc,&00,&fc,&f0,&8f,&0f,&0f,&ff
                        defb &cc,&77,&f0,&f7,&ff,&fc,&f1,&88
                        
                        defb &00,&33,&ef,&0f,&0f,&cc,&11,&8f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &ff,&ff,&ff,&ff,&cc,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&e7,&3f,&ff
                        defb &88,&00,&fc,&f0,&8f,&0f,&0f,&7f
                        defb &cc,&76,&f0,&f7,&ff,&ff,&f0,&88
                        
                        defb &00,&00,&ff,&8f,&1f,&cc,&11,&0f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &ff,&ff,&ff,&fc,&cc,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&f7,&ff,&cc
                        defb &00,&00,&fc,&f0,&ff,&ff,&ff,&ee
                        defb &44,&76,&f0,&e7,&0f,&1f,&f8,&cc
                        
                        defb &00,&00,&33,&ef,&3f,&ff,&99,&0f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &00,&00,&32,&f0,&e6,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&f7,&cc,&00
                        defb &00,&00,&fc,&f0,&bb,&ff,&ff,&ee
                        defb &00,&fc,&f0,&cf,&0f,&1f,&f8,&cc
                        
                        defb &77,&ff,&00,&ff,&ff,&f3,&99,&0f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &00,&00,&32,&f0,&e6,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&e6,&00,&00
                        defb &00,&00,&fc,&f0,&88,&33,&f0,&f3
                        defb &00,&fc,&f0,&cf,&0f,&1f,&f8,&c4
                        
                        defb &77,&ff,&88,&33,&fe,&f1,&99,&0f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &00,&00,&32,&f0,&e6,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&e6,&00,&00
                        defb &00,&00,&fc,&f0,&88,&11,&f8,&f3
                        defb &11,&f8,&f0,&cf,&0f,&1f,&f8,&e6
                        
                        defb &fe,&f1,&88,&33,&fc,&f1,&bb,&0f
                        defb &3f,&ff,&88,&cf,&0f,&dd,&f8,&f1
                        defb &00,&00,&33,&f0,&e6,&00,&00,&fe
                        defb &f0,&c4,&00,&11,&f8,&e6,&00,&00
                        defb &00,&00,&fc,&f1,&00,&11,&f8,&f1
                        defb &99,&f8,&f1,&8f,&0f,&1f,&f8,&e6
                        
                        defb &fc,&f1,&88,&11,&f0,&f1,&bb,&0f
                        defb &7f,&ff,&ff,&ff,&cf,&dd,&f8,&f1
                        defb &00,&00,&33,&f0,&e6,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&e2,&00,&00
                        defb &00,&00,&fc,&f1,&00,&11,&f8,&f1
                        defb &bb,&f8,&f1,&ff,&ff,&ff,&f8,&e2
                        
                        defb &fc,&f1,&88,&11,&f0,&f3,&bb,&3f
                        defb &fe,&f0,&f0,&ff,&ef,&dd,&f8,&f3
                        defb &77,&ff,&ff,&f0,&e2,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&e2,&77,&ff
                        defb &ff,&cc,&fc,&f1,&00,&00,&fc,&f0
                        defb &bb,&f0,&f1,&ff,&ff,&ff,&f8,&f3
                        
                        defb &fc,&ff,&ff,&99,&f0,&f3,&33,&ff
                        defb &fc,&f0,&f0,&f0,&ff,&99,&f8,&f3
                        defb &ff,&ff,&ff,&f0,&f3,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&f3,&ff,&ff
                        defb &ff,&cc,&fc,&f1,&00,&00,&fc,&f0
                        defb &dd,&f8,&f3,&00,&00,&11,&f8,&f3
                        
                        defb &fd,&ff,&ff,&ff,&f0,&f3,&33,&fe
                        defb &f0,&f0,&f0,&f0,&ff,&99,&f8,&f3
                        defb &0f,&0f,&3f,&f0,&f3,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&f3,&0f,&0f
                        defb &0f,&cc,&fc,&f1,&00,&00,&76,&f0
                        defb &dd,&f8,&f3,&00,&00,&11,&f8,&f7
                        
                        defb &f9,&8f,&0f,&1f,&f0,&f3,&33,&33
                        defb &f0,&f0,&f0,&f0,&ee,&11,&f8,&f3
                        defb &0f,&0f,&1f,&f8,&f3,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&f3,&0f,&0f
                        defb &0f,&cc,&fc,&f1,&00,&00,&76,&f0
                        defb &c4,&fc,&f3,&00,&00,&11,&f9,&ee
                        
                        defb &ff,&8f,&0f,&1f,&f0,&f3,&00,&33
                        defb &f0,&f0,&f0,&f1,&cc,&11,&f0,&f3
                        defb &0f,&0f,&1f,&f8,&f3,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&f3,&0f,&0f
                        defb &0f,&cc,&fc,&f1,&00,&00,&76,&f0
                        defb &e6,&76,&e6,&00,&00,&11,&fb,&88
                        
                        defb &ff,&8f,&0f,&3f,&f8,&f3,&00,&33
                        defb &f0,&f1,&ff,&ff,&cc,&11,&f1,&ff
                        defb &0f,&0f,&1f,&ff,&ff,&00,&00,&fc
                        defb &f0,&cc,&00,&11,&f8,&ff,&0f,&0f
                        defb &0f,&4c,&ff,&f9,&00,&00,&33,&f7
                        defb &ee,&33,&e6,&00,&00,&00,&ff,&00
                        
                        defb &33,&0f,&0f,&1f,&ff,&f7,&00,&33
                        defb &ff,&ff,&ff,&ff,&88,&11,&ff,&ef
                        defb &0f,&0f,&0f,&ff,&88,&00,&00,&ff
                        defb &ff,&cc,&00,&11,&ff,&ef,&0f,&0f
                        defb &0f,&4c,&ff,&ff,&00,&00,&33,&ff
                        defb &cc,&11,&ee,&00,&00,&00,&cc,&00
                        
                        defb &33,&cf,&0f,&0f,&ff,&ee,&00,&11
                        defb &ff,&ee,&00,&00,&00,&11,&ff,&0f
                        defb &0f,&0f,&0f,&9f,&88,&00,&00,&77
                        defb &ff,&cc,&00,&11,&ff,&8f,&0f,&7f
                        defb &ff,&cc,&11,&ff,&00,&00,&33,&88
                        defb &00,&00,&cc,&00,&00,&00,&00,&00
                        
                        defb &11,&ff,&ef,&0f,&6e,&88,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&11,&ff
                        defb &ff,&ff,&ff,&ff,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&11,&ff,&ff,&ff
                        defb &ff,&cc,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&33,&ff,&ff,&ee,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&11,&ff
                        defb &ff,&ff,&ff,&cc,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&11,&ff,&ff,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&11,&ff,&ee,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&ff,&ee
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&ff,&ee
                        defb &00,&11,&ff,&ff,&cc,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&33,&cc,&00
                        defb &00,&00,&00,&00,&00,&00,&77,&ff
                        defb &88,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &77,&ff,&ff,&ff,&88,&00,&fc,&e6
                        defb &00,&11,&ff,&ff,&cc,&33,&ff,&ff
                        defb &ff,&ff,&ff,&88,&77,&ff,&cc,&00
                        defb &00,&00,&00,&00,&33,&ff,&ff,&ff
                        defb &88,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &df,&ff,&7f,&ff,&88,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&33,&ff,&ff
                        defb &ff,&ff,&ff,&cc,&67,&0f,&4c,&00
                        defb &00,&00,&00,&77,&ff,&fe,&f0,&f1
                        defb &88,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&ff
                        defb &ff,&0f,&0f,&1f,&88,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&33,&0f,&0f
                        defb &0f,&3f,&f7,&6e,&23,&0f,&4c,&00
                        defb &00,&00,&00,&ff,&f0,&f0,&f0,&f1
                        defb &88,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fe
                        defb &f7,&0f,&0f,&1f,&88,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&0f,&0f
                        defb &0f,&7e,&f1,&ee,&23,&0f,&4c,&00
                        defb &00,&00,&00,&fc,&f0,&f0,&f0,&f1
                        defb &ff,&88,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&0f,&1f,&cc,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&8f,&0f
                        defb &0f,&fe,&f1,&ee,&23,&0f,&4c,&00
                        defb &00,&00,&00,&fc,&f0,&f0,&f0,&f1
                        defb &ff,&88,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fe
                        defb &f3,&0f,&0f,&1f,&ff,&88,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&8f,&0f
                        defb &3f,&fc,&f0,&ee,&23,&0f,&4c,&00
                        defb &00,&00,&33,&ff,&f8,&f0,&f0,&f3
                        defb &8f,&cc,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&0f,&0f,&1f,&ff,&88,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&ff,&ff
                        defb &ff,&f8,&f0,&e6,&23,&0f,&4c,&00
                        defb &00,&00,&77,&ff,&ff,&ff,&ff,&ff
                        defb &8f,&cc,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&ff,&ff,&ff,&f9,&88,&f8,&e6
                        defb &00,&00,&f8,&f0,&cc,&11,&ff,&00
                        defb &77,&f0,&f0,&f3,&23,&0f,&4c,&00
                        defb &00,&00,&67,&0f,&ff,&ff,&ff,&ef
                        defb &0f,&cc,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&ff,&77,&ff,&f9,&88,&f8,&e6
                        defb &00,&00,&f8,&f0,&cc,&00,&00,&00
                        defb &76,&f0,&f0,&e6,&23,&0f,&6e,&00
                        defb &00,&00,&67,&0f,&cc,&00,&00,&67
                        defb &0f,&cc,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&11,&f8,&f1,&88,&f8,&e6
                        defb &00,&00,&f8,&f0,&cc,&00,&00,&00
                        defb &fe,&f0,&f0,&cc,&23,&0f,&6e,&00
                        defb &00,&00,&47,&0f,&88,&00,&00,&67
                        defb &0f,&4c,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&11,&f0,&f0,&99,&f8,&e6
                        defb &00,&00,&fc,&f0,&cc,&00,&00,&11
                        defb &fc,&f0,&f1,&cc,&23,&0f,&6e,&00
                        defb &00,&00,&47,&0f,&88,&00,&00,&67
                        defb &0f,&4c,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&11,&f0,&f0,&99,&f8,&e6
                        defb &00,&00,&fc,&f0,&cc,&00,&00,&33
                        defb &f8,&f0,&f3,&88,&67,&0f,&6e,&00
                        defb &00,&00,&47,&0f,&88,&00,&00,&77
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&11,&f0,&f0,&dd,&f8,&f7
                        defb &00,&00,&fc,&f0,&cc,&00,&00,&77
                        defb &f0,&f0,&f7,&00,&67,&0f,&6e,&00
                        defb &00,&00,&47,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&33,&f0,&f0,&dd,&f8,&f7
                        defb &00,&00,&fc,&f0,&cc,&00,&00,&76
                        defb &f0,&f0,&ee,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&76
                        defb &f3,&00,&33,&f0,&f0,&dd,&f8,&f7
                        defb &00,&00,&fc,&f0,&cc,&00,&00,&fc
                        defb &f0,&f1,&cc,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fe
                        defb &f3,&ff,&ff,&fe,&f0,&dd,&f8,&f7
                        defb &00,&00,&fc,&f0,&cc,&00,&11,&f8
                        defb &f0,&f3,&88,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&ff,&ff,&fe,&f0,&dd,&f8,&f3
                        defb &00,&00,&fc,&f0,&cc,&00,&33,&f0
                        defb &f0,&f7,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&0f,&7e,&f0,&dd,&f8,&f3
                        defb &00,&00,&fc,&f0,&cc,&00,&76,&f0
                        defb &f0,&e6,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&0f,&7e,&ff,&dd,&f0,&f3
                        defb &00,&00,&fc,&f0,&cc,&00,&76,&f0
                        defb &f0,&cc,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&0f,&7f,&ff,&99,&f0,&f3
                        defb &00,&00,&fc,&f0,&cc,&00,&fc,&f0
                        defb &f1,&88,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&0f,&3f,&00,&11,&f0,&f3
                        defb &00,&00,&fc,&f0,&cc,&11,&f8,&f0
                        defb &f3,&00,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&00,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&0f,&3f,&ff,&00,&11,&f0,&f3
                        defb &00,&00,&fc,&f0,&cc,&33,&f0,&f0
                        defb &e6,&00,&00,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&00,&00,&00,&33
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&3f,&ff,&ee,&00,&33,&f0,&f3
                        defb &ff,&88,&fc,&f0,&cc,&76,&f0,&f0
                        defb &cc,&00,&00,&00,&67,&0f,&7f,&ff
                        defb &cc,&00,&cf,&1f,&ff,&ff,&ff,&ff
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&00,&fc
                        defb &f3,&ff,&cc,&00,&00,&33,&f0,&f7
                        defb &ff,&ff,&ff,&fc,&cc,&76,&f0,&f1
                        defb &cc,&00,&00,&00,&67,&0f,&7f,&ff
                        defb &ff,&ee,&cf,&3f,&ff,&ff,&ff,&ff
                        defb &0f,&6e,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&fc
                        defb &f3,&88,&00,&00,&00,&33,&f3,&ef
                        defb &0f,&0f,&ff,&fe,&cc,&fc,&f0,&f3
                        defb &88,&00,&00,&00,&67,&0f,&7e,&f0
                        defb &f7,&ee,&cf,&7f,&f0,&f0,&f0,&f3
                        defb &ff,&ee,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&fc
                        defb &f3,&00,&00,&00,&00,&33,&ff,&cf
                        defb &0f,&0f,&0f,&ff,&99,&f8,&f0,&f7
                        defb &ff,&ff,&ff,&88,&67,&0f,&fe,&f0
                        defb &f0,&e6,&ff,&fc,&f0,&f0,&f0,&f3
                        defb &ff,&cc,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&f8
                        defb &f3,&00,&00,&00,&00,&33,&ef,&0f
                        defb &0f,&0f,&0f,&ff,&99,&f8,&f0,&ff
                        defb &ff,&ff,&ff,&cc,&67,&3f,&fc,&f0
                        defb &f0,&e2,&ff,&fc,&f0,&f0,&f0,&f1
                        defb &ff,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&f8
                        defb &f3,&00,&00,&00,&00,&33,&33,&0f
                        defb &0f,&0f,&0f,&ee,&11,&fc,&f1,&cf
                        defb &0f,&0f,&0f,&cc,&67,&7f,&f0,&f0
                        defb &f0,&e6,&00,&fc,&f0,&f0,&f0,&f0
                        defb &f3,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&f8
                        defb &f3,&00,&00,&00,&00,&00,&33,&0f
                        defb &0f,&0f,&1f,&cc,&00,&fc,&f3,&8f
                        defb &0f,&0f,&0f,&cc,&67,&fe,&f0,&f0
                        defb &f0,&e6,&00,&fe,&f0,&f0,&f0,&f0
                        defb &f3,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&f8
                        defb &f3,&00,&00,&00,&00,&00,&33,&0f
                        defb &1f,&ff,&ff,&cc,&00,&fe,&f7,&0f
                        defb &0f,&0f,&0f,&cc,&77,&fe,&f0,&f0
                        defb &f0,&e6,&00,&ff,&ff,&fc,&f0,&f0
                        defb &f3,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&f8
                        defb &f3,&00,&00,&00,&00,&00,&33,&ff
                        defb &ff,&ff,&ff,&88,&00,&76,&ff,&0f
                        defb &0f,&0f,&0f,&cc,&77,&33,&f0,&f7
                        defb &ff,&ee,&00,&33,&ff,&ff,&ff,&fc
                        defb &f3,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&ff
                        defb &ff,&00,&00,&00,&00,&00,&11,&ff
                        defb &ee,&00,&00,&00,&00,&77,&ef,&0f
                        defb &0f,&0f,&0f,&cc,&00,&33,&ff,&ff
                        defb &ee,&00,&00,&00,&00,&77,&ff,&ff
                        defb &ee,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&ff
                        defb &ee,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&33,&bb,&ff
                        defb &ff,&ff,&ff,&cc,&00,&33,&ff,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        
                        defb &00,&00,&00,&00,&00,&00,&11,&ee
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&11,&00,&33
                        defb &ff,&ff,&ff,&88,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00
                        defb &00,&00,&00,&00,&00,&00,&00,&00




