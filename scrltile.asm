;; Hardware scrolling a tile-map
;;
;; Scroll rate is 1 CRTC char per frame
;; We are only drawing the right side in this example.
;; Each tile is the same size as a CRTC char
;; 
;; TODO: Draw tiles over entire screen to set it up
org &1000


km_read_key equ &bb1b
mc_wait_flyback equ &bd19
scr_set_mode equ &bc0e
txt_output	equ &bb5a

map_width equ 40*3
map_height equ 25

num_tiles equ 2
tile_width equ 2	;; width in bytes
tile_height equ 8	;; height of tile in lines

screen_width equ 40	;; number of crtc chars in the width of the screen
screen_width_bytes equ screen_width*2

screen_height equ 200

;; number of tiles we see horizontally
tiles_width equ screen_width_bytes/tile_width
;; number of tiles we see vertically
tiles_height equ screen_height/tile_height

start:
;; set the screen mode
ld a,1
call scr_set_mode

;; we generate some data; really it's best to use a tile map editor
;; and generate some nice graphics

ld hl,tile_gfx
ld a,&ff
call init_tile_gfx

ld hl,tile_gfx+(tile_width*tile_height)
ld a,&aa
call init_tile_gfx

ld hl,tile_gfx+((tile_width*tile_height)*2)
ld a,&55
call init_tile_gfx

;; init map; first clear it
ld hl,map
ld e,l
ld d,h
inc de
ld (hl),0
ld bc,map_width*map_height-1
ldir

;; put tile 1 every 8th position
ld hl,map
ld de,8
ld bc,map_width*map_height/8
initmap:
ld (hl),&1
add hl,de
dec bc
ld a,b
or c
jr nz,initmap


;; draw initial screen starting at left and drawing columns until we
;; reach the right side of the screen
;; 
;; we use our column drawing code and draw one column
;; for each crtc character in the width.
;;
;; after this is done we have drawn the entire screen

;; e.g. if the screen is 40 chars wide, we draw 40 columns

;; top-left is based on our map_x, map_y coordinates
ld hl,(map_coords)
call get_map_addr
push hl
pop ix
;; IX = starting map address of top-left

;; Due to the way we have organised our map:

;; column 0 is map_x, map_y
;; column 1 is map_x+1, map_y
;; column 2 is map_x+2, map_y etc
;; so we increment the map x as we draw each column
;; in order to get the correct column of tiles

;; initial scroll offset for drawing entire visible part of map
;; we use this so we draw the first and subsequent columns in the correct
;; place on the screen
ld hl,(scroll_offset)


;; number of tiles in the width of the screen
;; this is our loop
ld b,tiles_width
dis:
push bc
push ix
push hl

;; convert scroll offset to scr address
ld a,h
and &3
ld h,a
add hl,hl
ld a,(scroll_base)
add a,a
add a,a
or h
ld h,a
ex de,hl
;; DE = screen address for CRTC char at top of column
;; this char is at the top of the screen

;; draw an entire column
call draw_column

pop hl
;; update the offset for drawing initial screen
;; moving one CRTC char to the right
inc hl

pop ix
inc ix    ;; move to next map address moving accross the map 1 tile in X
        ;; one CRTC  char per map means we increment the map address
        ;; as we do each CRTC char. If the tiles were bigger we need to work out
        ;; which slice of the tile to draw and when we have drawn all slices of
        ;; a tile, we move to the next map address.

pop bc
djnz dis

loop:
;; wait for vsync 
;;
;; (synchronises scroll with the screen refresh; as a result it
;; updates at a constant rate and is smooth)
call mc_wait_flyback

;; write scroll parameters to hardware
call update_scroll

call set_scroll

ld hl,(map_coords)
;; if we are drawing the left column we don't add anything on
;; x,y defines the top of the column to draw
;;
;; if we are drawing the right column we add on the width of the screen - 1
;; we can do this because each map tile is one crtc char
ld a,h
add a,tiles_width-1
ld h,a

;; calculate the memory address in the map
call get_map_addr

;; transfer HL to IX
push hl
pop ix


;; calculate the memory address on-screen to draw to.
ld hl,(scroll_offset)
ld bc,screen_width-1      ;; width of screen in chars (R1-1)
add hl,bc
ld a,h
and &3
ld h,a
ld a,(scroll_base)
add a,a
add a,a
or h
ld h,a
ex de,hl

call draw_column

;; loop
jp loop

;; function to fill memory with a byte used to initialise tile graphics in this example
init_tile_gfx:
ld b,tile_width*tile_height
itg1:
ld (hl),a
inc hl
djnz itg1
ret


;;---------------------------------------------------------------------

;; Code assumes tile height is no taller than 8 lines

;; in
;; DE = screen address to write tile to
;; HL = address of tile graphics
;; out:
;; HL,DE modified

draw_tile:
;; each tile is stored so that the graphics for line 0 are stored first,
;; then line 1, then line 2, then line 3.

;; the following is expanded by pasmo
;; by the rept. all code between rept and endm
;; is repeated "tile_height" times
rept 8
ld a,(hl)
ld (de),a
inc e
inc hl
ld a,(hl)
ld (de),a
dec e
inc hl
ld a,d
add a,8
ld d,a
endm
ret

;;---------------------------------------------------------------------
;; in
;; A = tile index
;; out:
;; HL = tile graphics address
;; A is modified

;; tile is 2 bytes wide and 8 lines tall and stored uncompressed.
;; each tile's graphics is therefore 2*8 bytes = 16.
;; the graphics for each tile are stored after each other
;; in order. so that tile 1 comes after tile 0.
;; the start of the data is tile_gfx, this is the location of tile 0
;; tile 1 is therefore tile_gfx+16, tile 2 is at tile_gfx+32
;;
;; we could use a lookup table or we can compute
;; the location because the tile size is fixed
get_tile_addr:
ld a,l
ld h,0
;; HL = tile index
add hl,hl		;; x2
add hl,hl		;; x4
add hl,hl		;; x8
add hl,hl		;; x16
;; HL = offset from tile_gfx to the graphics for tile 'A'
ld a,l
add a,tile_gfx AND 255
ld l,a
ld a,h
adc a,tile_gfx/256
ld h,a
;; HL = memory address of tile graphics
ret

;;---------------------------------------------------------------------
;; convert a map x,y coordinate to a memory address
;;
;; in
;; H = map x
;; L = map y
;; out
;; HL = memory address of that position in the map

get_map_addr:
push de
push bc
ld b,l
ld c,h

ld hl,map

;; is B = 0?
inc b
dec b
jr z,gma2

;; this performs a multiply.
;; it's effectively map_y*map_width

ld de,map_width
gma1:
add hl,de
djnz gma1

gma2:
;; add on x coordinate
ld b,0
add hl,bc
pop bc
pop de
ret


;;---------------------------------------------------------------------
;; drawing a column involves drawing a tile
;; moving down the screen, drawing another tile
;;
;; our movement through the map is to move down a line at a time

;; IX = address in map of this column
;; DE = address on screen to draw this map to
draw_column:
ld b,tiles_height

dc1:
push bc
push de

ld a,(ix+0)			;; get tile index from map

;; to move to the next line, we need to add on the width of the map
ld bc,map_width
add ix,bc

call get_tile_addr		;; convert tile index to address in memory of tile's graphics
call draw_tile			;; draw the tile to the screen

pop hl

;; move down 8 lines - this code is not optimised!
ld b,8
dc2:
call scr_next_line
djnz dc2
ex de,hl

pop bc
djnz dc1
ret



;;---------------------------------------------------------------------
;; firmware function: SCR NEXT LINE
;;
;; Entry conditions:
;; HL = screen address
;; Exit conditions:
;; HL = updated screen address
;; AF corrupt
;;
;; Assumes:
;; - 16k screen
;; - 80 bytes per line (40 CRTC characters per line)

scr_next_line:
ld      a,h
add     a,8
ld      h,a


and     &38
ret     nz

;; 

ld      a,h
sub     &40
ld      h,a
ld      a,l
add     a,80			;; number of bytes per line
ld      l,a
ret     nc

inc     h
ld      a,h
and     &7
ret     nz

ld      a,h
sub     8
ld      h,a
ret     

;;--------------------------------------------------------------------------------------------

update_scroll:
ld hl,(scroll_offset)
inc hl
ld (scroll_offset),hl

;; move through the map
;; 1 CRTC char is 1 tile in the map
;; so when we update the scroll we also
;; move through the map
ld a,(map_x)
inc a
ld (map_x),a
ret

;;--------------------------------------------------------------------------------------------
;; set scroll parameters in hardware
set_scroll:
ld hl,(scroll_offset)

;; write scroll offset (in CRTC character width units)
ld bc,&bc0c				;; select CRTC register 12
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write

;; combine with scroll base
ld a,(scroll_base)
or h
out (c),a

ld bc,&bc0d				;; select CRTC register 13
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write
out (c),l
ret

;;--------------------------------------------------------------------------------------------


;; high byte of the screen base address
;; &00 -> screen uses &0000-&3fff
;; &10 -> screen uses &4000-&7fff
;; &20 -> screen uses &8000-&bfff
;; &30 -> screen uses &c000-&ffff

scroll_base: 
defb &30

;; the scroll offset in CRTC character units
scroll_offset:
defw 0

;;--------------------------------------------------------------------------------------------
map_coords:
;; this is the map coordinate for the top-right of the screen
map_y:
defb 0

;; this is the map coordinate for the top-left of the screen
map_x:
defb 0

;;--------------------------------------------------------------------------------------------

;; this is our tile map
;; each byte is 1 tile and contains the index of the tile which should
;; be shown in this position
;;
;; The memory is organised so we have first row of the map, then the
;; next row of the map, then the next etc.
;;
;; To work out a position in the map (map_y*map_width)+map_x
;; then add on "map" to get the memory address
;;
;; when we are scrolling we are viewing a region of the map
map:
defs map_width*map_height

;;--------------------------------------------------------------------------------------------
;; this is the graphics for all the tiles
;; tile 0 graphics comes first, then tile 1 then tile 2 etc without any
;; gaps between them. The tiles are stored in a form we can write directly
;; to the screen to display them. They are uncompressed.
tile_gfx:
defs tile_width*tile_height*num_tiles

end start