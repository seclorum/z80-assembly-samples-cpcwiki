;; plot a pixel using the firmware screen functions

scr_set_mode equ &bc0e
scr_dot_position equ &bc1d
scr_ink_encode equ &bc2c
kl_u_rom_enable equ &b900
txt_set_paper equ &bb96
txt_clear_window equ &bb6c

org &4000


;; uncomment to see that this causes an effect
;;call kl_u_rom_enable

;; set mode 0
xor a
call scr_set_mode	

;; set paper
ld a,1
call txt_set_paper

;; clears window to paper colour
call txt_clear_window

;; plot point
;;
;; for mode 0: x is in the range 0-159
;; for mode 1: x is in the range 0-320
;; for mode 2: x is in the range 0-639
;; for all modes: y is in the range 0-200
;; origin is bottom left

;; get address of pixel
;; change coords for mode 1 and 2 to get same location as mode 0
ld de,80		;; x 
ld hl,100		;; y 
call scr_dot_position
;; HL = address of byte containing pixel
;; C = bit mask for pixel
;; B = number of pixels in byte

push hl
push bc

ld a,3				;; pen 3
call scr_ink_encode
;; A = pen 3 encoded into a form to poke to screen
;; but which would set all pixels in that byte

pop bc
pop hl
;; isolate just the pixel we want
;; using the pixel mask
and c
ld d,a


;; this avoids SCR PIXELS, but it is more complex.
;; We also require screen memory to be readable and writeable.
;; e.g. upper rom can't be enabled.
;;
;; SCR PIXELS does all the work necessary to enable/disable rom to access
;; screen memory in range &c000-&ffff.
;;


;; we need to read from screen
;; mask the pixels we don't want to change
;; combine with our pixel and write back

;; mask for pixel we want to write
ld a,c

;; complement it to make mask for pixels to keep
cpl

;; isolate pixels on screen 
and (hl)

;; combine with our pixel
or d

;; write to screen
ld (hl),a

ret
