;; Smooth vertical scroll (no rupture)
;;
;; This code demonstrates a method to scroll the screen, using the hardware,
;; vertically line by line.
;;
;; The standard hardware scroll method, using register 12 and 13 alone, will
;; scroll the screen at a rate of 8 scan lines vertically.
;; 
;; 8 is the number of scanlines defined by R9+1.
;;
;; So this method provides a method for smoother scroll in the vertical
;; direction.
;;  
;; The effect relies on the reaction of the monitor to the vertical sync from 
;; the CRTC and the change in frame time so it is only guaranteed to work properly 
;; on a real Amstrad CPC  with an Amstrad monitor.
;;
;;
;; Use Q and A to scroll the screen up and down.
;; 
;; This scroll method can be used on CPC,CPC+ and KC Compact.

;; the position of this code is not important
org &4000


.km_read_key equ &bb1b
.mc_wait_flyback equ &bd19
.scr_set_mode equ &bc0e
.txt_output	equ &bb5a

;; set the screen mode
ld a,1
call scr_set_mode

ld bc,24*40
ld d,' '
.l1
inc d
ld a,d
cp &7f
jr nz,no_char_reset
ld d,' '
no_char_reset:
ld a,d
call txt_output
dec bc
ld a,b
or c
jr nz,l1

.loop
;; wait for vsync 
;;
;; (synchronises scroll with the screen refresh; as a result it
;; updates at a constant rate and is smooth)
call mc_wait_flyback

;; write scroll parameters to hardware
call update_scroll

;; check if any keys are pressed (they will update the scroll offset)
call check_keys

;; delay long enough for the vsync to become inactive 
;; so that mc_wait_flyback will synchronise with the start of the 
;; vsync.
halt

halt

;; loop
jp loop

;;----------------------------------------------------------------------
.check_keys
;; test if any key has been pressed
call km_read_key
ret nc
;; A = code of the key that has been pressed
;;
;; check the codes we are using and handle appropiatly.
cp 'Q'					; Q
jp z,scroll_up
cp 'q'
jp z,scroll_up
cp 'A'					; A
jp z,scroll_down
cp 'a'
jp z,scroll_down
ret

;;----------------------------------------------------------------------
.scroll_up
ld a,(vert_adjustment)
dec a
and &7
ld (vert_adjustment),a
or a
ret z

;; get current scroll offset
ld hl,(scroll_offset)
;; subtract width of screen
or a
ld bc,40
sbc hl,bc

;; ensure scroll offset is in range &000-&3ff
ld a,h
and &3
ld h,a

;; store new scroll offset. It is now ready to be written to the CRTC.
ld (scroll_offset),hl
ret

;;----------------------------------------------------------------------

.scroll_down
ld a,(vert_adjustment)
inc a
and &7
ld (vert_adjustment),a
or a
ret z
;; get current scroll offset
ld hl,(scroll_offset)
;; add on width of screen
ld bc,40
add hl,bc
;; ensure scroll offset is in range &000-&3ff
ld a,h
and &3
ld h,a

;; store new scroll offset. It is now ready to be written to the CRTC.
ld (scroll_offset),hl
ret


;;--------------------------------------------------------------------------------------------

.update_scroll
;; write scroll adjustment
ld a,(vert_adjustment)
ld bc,&bc05			;; select CRTC register 5 (vertical adjust)
out (c),c
inc b
out (c),a			

ld hl,(scroll_offset)

;; write scroll offset (in CRTC character width units)
ld bc,&bc0c				;; select CRTC register 12
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write

;; combine with scroll base
ld a,(scroll_base)
or h
out (c),a

ld bc,&bc0d				;; select CRTC register 13
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write
out (c),l
ret





;;----------------------------------------------------------------------
;; scroll parameters

.vert_adjustment defb 0

;; high byte of the screen base address
;; &00 -> screen uses &0000-&3fff
;; &10 -> screen uses &4000-&7fff
;; &20 -> screen uses &8000-&bfff
;; &30 -> screen uses &c000-&ffff

.scroll_base defb &30

;; the scroll offset in CRTC character units
.scroll_offset defw 0
