;; Sprite Decompression Routine
;; Decompresses a compressed sprite directly to the screen without using a buffer
;; by redbox - October 2013
;; http://www.cpcwiki.eu/index.php/User:Redbox

			org	&4000

			compressed_sprite	equ	&6000
			sprite_width		equ	48	; bytes
			sprite_height		equ	75	; lines
			control_code		equ	&80



			ld 	hl,compressed_sprite
			ld 	de,&c000

			ld	b,sprite_height

loop:			push	bc				; store counter
			push	de				; store screen address
			call	line				; decompress one line
			pop	de				; restore screen address
			pop	bc				; restore counter
			ex	de,hl
			call	next_line			; calculate next screen address line down
			ex	de,hl
			djnz	loop				; and loop

			ret



line:			ld	b,sprite_width			; loop counter (width in bytes)

line_loop:		ld 	a,(hl)				; get byte
			cp 	control_code			; is it control code?
			jr 	z,control_code_found		; yes, then print repetitions
			ld 	(de),a				; else just print byte
			inc 	hl			
			inc 	de
			djnz	line_loop			; and loop
			ret

control_code_found:	inc 	hl
			ld 	a,(hl)				; get byte to repeat
			inc 	hl
			ld 	c,(hl)				; get number of repetitions
			push	bc				; store counters
repeat_loop:		ld 	(de),a				; put it on screen
			inc 	de
			dec	c
			jr	nz,repeat_loop			; repeat until done
			pop	bc				; restore counters
			ld	a,b
			add	a,1				; add 1 to it (compressed data is stored without 0 being counted)
			sub	c				; subtract number of repetitions from overall loop counter
			ld	b,a				; put it back in B
			inc 	hl				; next byte to do in queue
			djnz	line_loop			; and loop
			ret



next_line:		ld 	a,8
        		add 	a,h   				; add 8 to H (the high byte)
        		ld 	h,a
	       		ret 	nc         			; return if no overflow

        		push 	de         			; otherwise preserve DE
        		ld 	de,&C050
        		add 	hl,de       			; add &C000+&50 to HL
        		pop 	de          			; and get DE back again
        		ret



			org	&6000

                        defb &80,&00,&0e,&11,&ee,&80,&00,&0d
                        defb &ff,&ff,&ff,&cc,&80,&00,&0f,&00
                        defb &33,&ff,&ff,&88,&00,&00,&ff,&ee
                        defb &80,&00,&05,&33,&80,&ff,&04,&88
                        defb &80,&00,&08,&11,&80,&ff,&04,&88
                        defb &80,&00,&09,&11,&80,&00,&04,&00
                        defb &33,&ff,&ff,&ff,&cc,&00,&ff,&ee
                        defb &00,&11,&ff,&ff,&cc,&33,&0f,&7f
                        defb &ff,&ff,&cc,&80,&00,&07,&11,&ff
                        defb &fe,&f0,&f0,&f1,&88,&00,&77,&ff
                        defb &ff,&cc,&80,&00,&04,&33,&88,&cc
                        defb &00,&00,&00,&77,&cf,&0f,&1f,&ff
                        defb &00,&cf,&6e,&00,&11,&ff,&ff,&cc
                        defb &ff,&ff,&0f,&0f,&0f,&cc,&80,&00
                        defb &07,&33,&ff,&fe,&f0,&f0,&f1,&88
                        defb &00,&77,&80,&ff,&04,&88,&00,&00
                        defb &33,&ff,&ee,&00,&00,&00,&ff,&ef
                        defb &0f,&0f,&1f,&00,&cf,&6e,&00,&11
                        defb &8f,&0f,&dd,&ff,&ff,&0f,&0f,&0f
                        defb &ee,&00,&11,&80,&ff,&04,&dd,&ff
                        defb &f0,&f6,&f0,&f0,&f1,&88,&77,&ff
                        defb &0f,&0f,&7f,&ff,&88,&00,&00,&77
                        defb &f7,&e6,&00,&00,&77,&fe,&c7,&0f
                        defb &0f,&1f,&00,&cf,&6e,&00,&11,&8f
                        defb &0f,&dd,&f8,&f1,&8f,&0f,&0f,&ff
                        defb &00,&33,&80,&ff,&06,&f0,&f6,&f0
                        defb &f0,&f1,&88,&ff,&ff,&cf,&0f,&0f
                        defb &1f,&88,&00,&00,&76,&f3,&f3,&00
                        defb &00,&76,&f0,&cf,&0f,&0f,&1f,&00
                        defb &cf,&6e,&00,&11,&8f,&0f,&dd,&f8
                        defb &f1,&8f,&0f,&0f,&7d,&88,&67,&80
                        defb &0f,&05,&ff,&f0,&f6,&f0,&f0,&f1
                        defb &88,&fe,&f0,&cf,&0f,&0f,&1f,&88
                        defb &00,&00,&fe,&f3,&f3,&00,&00,&76
                        defb &f0,&cf,&0f,&0f,&3f,&cc,&cf,&6e
                        defb &00,&11,&8f,&0f,&dd,&f8,&f1,&8f
                        defb &0f,&0f,&7d,&88,&67,&80,&0f,&05
                        defb &ff,&f0,&f7,&ff,&ff,&ff,&88,&74
                        defb &f0,&cf,&0f,&0f,&1f,&cc,&00,&00
                        defb &fc,&f3,&f3,&88,&00,&76,&f0,&ff
                        defb &ff,&ff,&3f,&cc,&cf,&6e,&00,&11
                        defb &8f,&0f,&dd,&f8,&f1,&ff,&ff,&ff
                        defb &fd,&88,&67,&80,&0f,&05,&ff,&f0
                        defb &f7,&ff,&ff,&ff,&88,&74,&f0,&cf
                        defb &0f,&0f,&1f,&ff,&00,&11,&fc,&f6
                        defb &f1,&88,&00,&74,&f0,&ee,&ff,&ff
                        defb &ff,&cc,&cf,&6e,&00,&11,&8f,&0f
                        defb &dd,&f8,&f1,&00,&ff,&ff,&f8,&cc
                        defb &67,&80,&0f,&05,&ff,&f0,&e6,&80
                        defb &00,&04,&74,&f0,&80,&ff,&05,&00
                        defb &11,&f8,&f6,&f1,&88,&00,&fc,&f0
                        defb &88,&00,&76,&f0,&cc,&8f,&6e,&00
                        defb &00,&8f,&0f,&dd,&f8,&f1,&00,&00
                        defb &76,&f0,&cc,&67,&80,&0f,&05,&ff
                        defb &f0,&e6,&80,&00,&04,&74,&f0,&cc
                        defb &77,&ff,&ff,&f3,&00,&33,&f0,&f6
                        defb &f0,&cc,&00,&fc,&f0,&88,&00,&76
                        defb &f0,&cc,&8f,&6e,&00,&00,&8f,&0f
                        defb &dd,&f8,&f1,&00,&00,&76,&f0,&cc
                        defb &77,&ff,&ff,&cf,&3f,&ff,&dd,&f8
                        defb &e6,&80,&00,&04,&74,&f0,&cc,&00
                        defb &11,&f8,&f3,&00,&33,&f0,&fe,&f0
                        defb &cc,&00,&fc,&f1,&88,&00,&77,&f0
                        defb &cc,&8f,&6e,&00,&00,&8f,&0f,&dd
                        defb &f8,&f1,&00,&00,&33,&f0,&ee,&33
                        defb &80,&ff,&04,&cc,&11,&f8,&e6,&00
                        defb &ff,&cc,&00,&74,&f0,&cc,&00,&33
                        defb &f8,&e2,&00,&76,&f0,&fe,&f0,&ee
                        defb &00,&fc,&f1,&cc,&00,&77,&ff,&dd
                        defb &8f,&6e,&00,&00,&cf,&0f,&dd,&f8
                        defb &f1,&00,&00,&33,&f3,&ee,&00,&00
                        defb &76,&ff,&cc,&00,&11,&f8,&f7,&ff
                        defb &ff,&cc,&00,&74,&f0,&cc,&00,&33
                        defb &f0,&e2,&00,&76,&f0,&ff,&f0,&e6
                        defb &00,&fc,&f1,&ff,&00,&11,&ff,&99
                        defb &8f,&6e,&00,&00,&cf,&0f,&dd,&f8
                        defb &f1,&80,&ff,&04,&cc,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&f7,&cf,&0f
                        defb &6e,&00,&74,&f0,&cc,&00,&33,&f0
                        defb &e2,&00,&fc,&f1,&bb,&f0,&e6,&00
                        defb &fc,&f1,&3f,&cc,&00,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &ff,&8f,&ef,&ee,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&dd,&ff,&ff,&f0
                        defb &e2,&11,&fc,&f1,&99,&f0,&f3,&00
                        defb &fc,&f3,&0f,&ff,&88,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&ff,&ff,&ff,&f0
                        defb &e6,&11,&f8,&f1,&99,&f8,&f3,&00
                        defb &ff,&ff,&0f,&1f,&ff,&00,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&8f,&0f,&3f,&f0
                        defb &e6,&33,&f8,&f3,&11,&f8,&f3,&00
                        defb &ff,&ef,&0f,&0f,&7f,&cc,&11,&8f
                        defb &7f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&cc,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&0f
                        defb &6e,&00,&74,&f0,&8f,&0f,&3f,&fc
                        defb &e6,&33,&f0,&f3,&11,&f8,&f1,&88
                        defb &00,&ff,&8f,&0f,&0f,&cc,&11,&8f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &0f,&0f,&0f,&ff,&00,&00,&00,&76
                        defb &f0,&c4,&00,&11,&f8,&e7,&0f,&1f
                        defb &cc,&00,&fc,&f0,&8f,&0f,&0f,&ff
                        defb &cc,&77,&f0,&f7,&ff,&fc,&f1,&88
                        defb &00,&33,&ef,&0f,&0f,&cc,&11,&8f
                        defb &3f,&00,&00,&cf,&0f,&dd,&f8,&f1
                        defb &80,&ff,&04,&cc,&00,&00,&fe,&f0
                        defb &c4,&00,&11,&f8,&e7,&3f,&ff,&88
                        defb &00,&fc,&f0,&8f,&0f,&0f,&7f,&cc
                        defb &76,&f0,&f7,&ff,&ff,&f0,&88,&00
                        defb &00,&ff,&8f,&1f,&cc,&11,&0f,&3f
                        defb &00,&00,&cf,&0f,&dd,&f8,&f1,&ff
                        defb &ff,&ff,&fc,&cc,&00,&00,&fe,&f0
                        defb &c4,&00,&11,&f8,&f7,&ff,&cc,&00
                        defb &00,&fc,&f0,&ff,&ff,&ff,&ee,&44
                        defb &76,&f0,&e7,&0f,&1f,&f8,&cc,&00
                        defb &00,&33,&ef,&3f,&ff,&99,&0f,&3f
                        defb &00,&00,&cf,&0f,&dd,&f8,&f1,&00
                        defb &00,&32,&f0,&e6,&00,&00,&fe,&f0
                        defb &c4,&00,&11,&f8,&f7,&cc,&00,&00
                        defb &00,&fc,&f0,&bb,&ff,&ff,&ee,&00
                        defb &fc,&f0,&cf,&0f,&1f,&f8,&cc,&77
                        defb &ff,&00,&ff,&ff,&f3,&99,&0f,&3f
                        defb &00,&00,&cf,&0f,&dd,&f8,&f1,&00
                        defb &00,&32,&f0,&e6,&00,&00,&fe,&f0
                        defb &c4,&00,&11,&f8,&e6,&80,&00,&04
                        defb &fc,&f0,&88,&33,&f0,&f3,&00,&fc
                        defb &f0,&cf,&0f,&1f,&f8,&c4,&77,&ff
                        defb &88,&33,&fe,&f1,&99,&0f,&3f,&00
                        defb &00,&cf,&0f,&dd,&f8,&f1,&00,&00
                        defb &32,&f0,&e6,&00,&00,&fe,&f0,&c4
                        defb &00,&11,&f8,&e6,&80,&00,&04,&fc
                        defb &f0,&88,&11,&f8,&f3,&11,&f8,&f0
                        defb &cf,&0f,&1f,&f8,&e6,&fe,&f1,&88
                        defb &33,&fc,&f1,&bb,&0f,&3f,&ff,&88
                        defb &cf,&0f,&dd,&f8,&f1,&00,&00,&33
                        defb &f0,&e6,&00,&00,&fe,&f0,&c4,&00
                        defb &11,&f8,&e6,&80,&00,&04,&fc,&f1
                        defb &00,&11,&f8,&f1,&99,&f8,&f1,&8f
                        defb &0f,&1f,&f8,&e6,&fc,&f1,&88,&11
                        defb &f0,&f1,&bb,&0f,&7f,&ff,&ff,&ff
                        defb &cf,&dd,&f8,&f1,&00,&00,&33,&f0
                        defb &e6,&00,&00,&fc,&f0,&cc,&00,&11
                        defb &f8,&e2,&80,&00,&04,&fc,&f1,&00
                        defb &11,&f8,&f1,&bb,&f8,&f1,&ff,&ff
                        defb &ff,&f8,&e2,&fc,&f1,&88,&11,&f0
                        defb &f3,&bb,&3f,&fe,&f0,&f0,&ff,&ef
                        defb &dd,&f8,&f3,&77,&ff,&ff,&f0,&e2
                        defb &00,&00,&fc,&f0,&cc,&00,&11,&f8
                        defb &e2,&77,&ff,&ff,&cc,&fc,&f1,&00
                        defb &00,&fc,&f0,&bb,&f0,&f1,&ff,&ff
                        defb &ff,&f8,&f3,&fc,&ff,&ff,&99,&f0
                        defb &f3,&33,&ff,&fc,&f0,&f0,&f0,&ff
                        defb &99,&f8,&f3,&ff,&ff,&ff,&f0,&f3
                        defb &00,&00,&fc,&f0,&cc,&00,&11,&f8
                        defb &f3,&ff,&ff,&ff,&cc,&fc,&f1,&00
                        defb &00,&fc,&f0,&dd,&f8,&f3,&00,&00
                        defb &11,&f8,&f3,&fd,&ff,&ff,&ff,&f0
                        defb &f3,&33,&fe,&80,&f0,&04,&ff,&99
                        defb &f8,&f3,&0f,&0f,&3f,&f0,&f3,&00
                        defb &00,&fc,&f0,&cc,&00,&11,&f8,&f3
                        defb &0f,&0f,&0f,&cc,&fc,&f1,&00,&00
                        defb &76,&f0,&dd,&f8,&f3,&00,&00,&11
                        defb &f8,&f7,&f9,&8f,&0f,&1f,&f0,&f3
                        defb &33,&33,&80,&f0,&04,&ee,&11,&f8
                        defb &f3,&0f,&0f,&1f,&f8,&f3,&00,&00
                        defb &fc,&f0,&cc,&00,&11,&f8,&f3,&0f
                        defb &0f,&0f,&cc,&fc,&f1,&00,&00,&76
                        defb &f0,&c4,&fc,&f3,&00,&00,&11,&f9
                        defb &ee,&ff,&8f,&0f,&1f,&f0,&f3,&00
                        defb &33,&f0,&f0,&f0,&f1,&cc,&11,&f0
                        defb &f3,&0f,&0f,&1f,&f8,&f3,&00,&00
                        defb &fc,&f0,&cc,&00,&11,&f8,&f3,&0f
                        defb &0f,&0f,&cc,&fc,&f1,&00,&00,&76
                        defb &f0,&e6,&76,&e6,&00,&00,&11,&fb
                        defb &88,&ff,&8f,&0f,&3f,&f8,&f3,&00
                        defb &33,&f0,&f1,&ff,&ff,&cc,&11,&f1
                        defb &ff,&0f,&0f,&1f,&ff,&ff,&00,&00
                        defb &fc,&f0,&cc,&00,&11,&f8,&ff,&0f
                        defb &0f,&0f,&4c,&ff,&f9,&00,&00,&33
                        defb &f7,&ee,&33,&e6,&00,&00,&00,&ff
                        defb &00,&33,&0f,&0f,&1f,&ff,&f7,&00
                        defb &33,&80,&ff,&04,&88,&11,&ff,&ef
                        defb &0f,&0f,&0f,&ff,&88,&00,&00,&ff
                        defb &ff,&cc,&00,&11,&ff,&ef,&0f,&0f
                        defb &0f,&4c,&ff,&ff,&00,&00,&33,&ff
                        defb &cc,&11,&ee,&00,&00,&00,&cc,&00
                        defb &33,&cf,&0f,&0f,&ff,&ee,&00,&11
                        defb &ff,&ee,&00,&00,&00,&11,&ff,&80
                        defb &0f,&04,&9f,&88,&00,&00,&77,&ff
                        defb &cc,&00,&11,&ff,&8f,&0f,&7f,&ff
                        defb &cc,&11,&ff,&00,&00,&33,&88,&00
                        defb &00,&cc,&80,&00,&05,&11,&ff,&ef
                        defb &0f,&6e,&88,&80,&00,&08,&11,&80
                        defb &ff,&05,&80,&00,&08,&11,&80,&ff
                        defb &04,&cc,&80,&00,&0e,&00,&33,&ff
                        defb &ff,&ee,&80,&00,&09,&11,&80,&ff
                        defb &04,&cc,&80,&00,&08,&11,&ff,&ff
                        defb &80,&00,&11,&00,&00,&11,&ff,&ee
                        defb &80,&00,&2b,&80,&00,&30,&80,&00
                        defb &0e,&ff,&ee,&80,&00,&20,&80,&00
                        defb &0e,&ff,&ee,&00,&11,&ff,&ff,&cc
                        defb &80,&00,&08,&33,&cc,&80,&00,&07
                        defb &77,&ff,&88,&80,&00,&07,&80,&00
                        defb &08,&77,&ff,&ff,&ff,&88,&00,&fc
                        defb &e6,&00,&11,&ff,&ff,&cc,&33,&80
                        defb &ff,&05,&88,&77,&ff,&cc,&80,&00
                        defb &05,&33,&ff,&ff,&ff,&88,&80,&00
                        defb &07,&80,&00,&08,&df,&ff,&7f,&ff
                        defb &88,&00,&fc,&e6,&00,&11,&f8,&f0
                        defb &cc,&33,&80,&ff,&05,&cc,&67,&0f
                        defb &4c,&80,&00,&04,&77,&ff,&fe,&f0
                        defb &f1,&88,&80,&00,&07,&80,&00,&07
                        defb &ff,&ff,&0f,&0f,&1f,&88,&00,&fc
                        defb &e6,&00,&11,&f8,&f0,&cc,&33,&0f
                        defb &0f,&0f,&3f,&f7,&6e,&23,&0f,&4c
                        defb &80,&00,&04,&ff,&f0,&f0,&f0,&f1
                        defb &88,&80,&00,&07,&80,&00,&07,&fe
                        defb &f7,&0f,&0f,&1f,&88,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&0f,&0f
                        defb &0f,&7e,&f1,&ee,&23,&0f,&4c,&80
                        defb &00,&04,&fc,&f0,&f0,&f0,&f1,&ff
                        defb &88,&80,&00,&06,&80,&00,&07,&fc
                        defb &f3,&0f,&0f,&1f,&cc,&00,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&8f,&0f
                        defb &0f,&fe,&f1,&ee,&23,&0f,&4c,&80
                        defb &00,&04,&fc,&f0,&f0,&f0,&f1,&ff
                        defb &88,&80,&00,&06,&80,&00,&07,&fe
                        defb &f3,&0f,&0f,&1f,&ff,&88,&fc,&e6
                        defb &00,&11,&f8,&f0,&cc,&11,&8f,&0f
                        defb &3f,&fc,&f0,&ee,&23,&0f,&4c,&00
                        defb &00,&00,&33,&ff,&f8,&f0,&f0,&f3
                        defb &8f,&cc,&80,&00,&06,&80,&00,&07
                        defb &76,&f3,&0f,&0f,&1f,&ff,&88,&fc
                        defb &e6,&00,&11,&f8,&f0,&cc,&11,&ff
                        defb &ff,&ff,&f8,&f0,&e6,&23,&0f,&4c
                        defb &00,&00,&00,&77,&80,&ff,&05,&8f
                        defb &cc,&80,&00,&06,&80,&00,&07,&76
                        defb &f3,&ff,&ff,&ff,&f9,&88,&f8,&e6
                        defb &00,&00,&f8,&f0,&cc,&11,&ff,&00
                        defb &77,&f0,&f0,&f3,&23,&0f,&4c,&00
                        defb &00,&00,&67,&0f,&ff,&ff,&ff,&ef
                        defb &0f,&cc,&80,&00,&06,&80,&00,&07
                        defb &76,&f3,&ff,&77,&ff,&f9,&88,&f8
                        defb &e6,&00,&00,&f8,&f0,&cc,&00,&00
                        defb &00,&76,&f0,&f0,&e6,&23,&0f,&6e
                        defb &00,&00,&00,&67,&0f,&cc,&00,&00
                        defb &67,&0f,&cc,&80,&00,&06,&80,&00
                        defb &07,&76,&f3,&00,&11,&f8,&f1,&88
                        defb &f8,&e6,&00,&00,&f8,&f0,&cc,&00
                        defb &00,&00,&fe,&f0,&f0,&cc,&23,&0f
                        defb &6e,&00,&00,&00,&47,&0f,&88,&00
                        defb &00,&67,&0f,&4c,&80,&00,&06,&80
                        defb &00,&07,&76,&f3,&00,&11,&f0,&f0
                        defb &99,&f8,&e6,&00,&00,&fc,&f0,&cc
                        defb &00,&00,&11,&fc,&f0,&f1,&cc,&23
                        defb &0f,&6e,&00,&00,&00,&47,&0f,&88
                        defb &00,&00,&67,&0f,&4c,&80,&00,&06
                        defb &80,&00,&07,&76,&f3,&00,&11,&f0
                        defb &f0,&99,&f8,&e6,&00,&00,&fc,&f0
                        defb &cc,&00,&00,&33,&f8,&f0,&f3,&88
                        defb &67,&0f,&6e,&00,&00,&00,&47,&0f
                        defb &88,&00,&00,&77,&0f,&6e,&80,&00
                        defb &06,&80,&00,&07,&76,&f3,&00,&11
                        defb &f0,&f0,&dd,&f8,&f7,&00,&00,&fc
                        defb &f0,&cc,&00,&00,&77,&f0,&f0,&f7
                        defb &00,&67,&0f,&6e,&00,&00,&00,&47
                        defb &1f,&88,&00,&00,&33,&0f,&6e,&80
                        defb &00,&06,&80,&00,&07,&76,&f3,&00
                        defb &33,&f0,&f0,&dd,&f8,&f7,&00,&00
                        defb &fc,&f0,&cc,&00,&00,&76,&f0,&f0
                        defb &ee,&00,&67,&0f,&6e,&00,&00,&00
                        defb &cf,&1f,&88,&00,&00,&33,&0f,&6e
                        defb &80,&00,&06,&80,&00,&07,&76,&f3
                        defb &00,&33,&f0,&f0,&dd,&f8,&f7,&00
                        defb &00,&fc,&f0,&cc,&00,&00,&fc,&f0
                        defb &f1,&cc,&00,&67,&0f,&6e,&00,&00
                        defb &00,&cf,&1f,&88,&00,&00,&33,&0f
                        defb &6e,&80,&00,&06,&80,&00,&07,&fe
                        defb &f3,&ff,&ff,&fe,&f0,&dd,&f8,&f7
                        defb &00,&00,&fc,&f0,&cc,&00,&11,&f8
                        defb &f0,&f3,&88,&00,&67,&0f,&6e,&00
                        defb &00,&00,&cf,&1f,&88,&00,&00,&33
                        defb &0f,&6e,&80,&00,&06,&80,&00,&07
                        defb &fc,&f3,&ff,&ff,&fe,&f0,&dd,&f8
                        defb &f3,&00,&00,&fc,&f0,&cc,&00,&33
                        defb &f0,&f0,&f7,&00,&00,&67,&0f,&6e
                        defb &00,&00,&00,&cf,&1f,&88,&00,&00
                        defb &33,&0f,&6e,&80,&00,&06,&80,&00
                        defb &07,&fc,&f3,&0f,&0f,&7e,&f0,&dd
                        defb &f8,&f3,&00,&00,&fc,&f0,&cc,&00
                        defb &76,&f0,&f0,&e6,&00,&00,&67,&0f
                        defb &6e,&00,&00,&00,&cf,&1f,&88,&00
                        defb &00,&33,&0f,&6e,&80,&00,&06,&80
                        defb &00,&07,&fc,&f3,&0f,&0f,&7e,&ff
                        defb &dd,&f0,&f3,&00,&00,&fc,&f0,&cc
                        defb &00,&76,&f0,&f0,&cc,&00,&00,&67
                        defb &0f,&6e,&00,&00,&00,&cf,&1f,&88
                        defb &00,&00,&33,&0f,&6e,&80,&00,&06
                        defb &80,&00,&07,&fc,&f3,&0f,&0f,&7f
                        defb &ff,&99,&f0,&f3,&00,&00,&fc,&f0
                        defb &cc,&00,&fc,&f0,&f1,&88,&00,&00
                        defb &67,&0f,&6e,&00,&00,&00,&cf,&1f
                        defb &88,&00,&00,&33,&0f,&6e,&80,&00
                        defb &06,&80,&00,&07,&fc,&f3,&0f,&0f
                        defb &3f,&00,&11,&f0,&f3,&00,&00,&fc
                        defb &f0,&cc,&11,&f8,&f0,&f3,&00,&00
                        defb &00,&67,&0f,&6e,&00,&00,&00,&cf
                        defb &1f,&00,&00,&00,&33,&0f,&6e,&80
                        defb &00,&06,&80,&00,&07,&fc,&f3,&0f
                        defb &3f,&ff,&00,&11,&f0,&f3,&00,&00
                        defb &fc,&f0,&cc,&33,&f0,&f0,&e6,&00
                        defb &00,&00,&67,&0f,&6e,&00,&00,&00
                        defb &cf,&1f,&00,&00,&00,&33,&0f,&6e
                        defb &80,&00,&06,&80,&00,&07,&fc,&f3
                        defb &3f,&ff,&ee,&00,&33,&f0,&f3,&ff
                        defb &88,&fc,&f0,&cc,&76,&f0,&f0,&cc
                        defb &00,&00,&00,&67,&0f,&7f,&ff,&cc
                        defb &00,&cf,&1f,&80,&ff,&04,&0f,&6e
                        defb &80,&00,&06,&80,&00,&07,&fc,&f3
                        defb &ff,&cc,&00,&00,&33,&f0,&f7,&ff
                        defb &ff,&ff,&fc,&cc,&76,&f0,&f1,&cc
                        defb &00,&00,&00,&67,&0f,&7f,&ff,&ff
                        defb &ee,&cf,&3f,&80,&ff,&04,&0f,&6e
                        defb &80,&00,&06,&80,&00,&06,&11,&fc
                        defb &f3,&88,&00,&00,&00,&33,&f3,&ef
                        defb &0f,&0f,&ff,&fe,&cc,&fc,&f0,&f3
                        defb &88,&00,&00,&00,&67,&0f,&7e,&f0
                        defb &f7,&ee,&cf,&7f,&f0,&f0,&f0,&f3
                        defb &ff,&ee,&80,&00,&06,&80,&00,&06
                        defb &11,&fc,&f3,&80,&00,&04,&33,&ff
                        defb &cf,&0f,&0f,&0f,&ff,&99,&f8,&f0
                        defb &f7,&ff,&ff,&ff,&88,&67,&0f,&fe
                        defb &f0,&f0,&e6,&ff,&fc,&f0,&f0,&f0
                        defb &f3,&ff,&cc,&80,&00,&06,&80,&00
                        defb &06,&11,&f8,&f3,&80,&00,&04,&33
                        defb &ef,&80,&0f,&04,&ff,&99,&f8,&f0
                        defb &80,&ff,&04,&cc,&67,&3f,&fc,&f0
                        defb &f0,&e2,&ff,&fc,&f0,&f0,&f0,&f1
                        defb &ff,&80,&00,&07,&80,&00,&06,&11
                        defb &f8,&f3,&80,&00,&04,&33,&33,&80
                        defb &0f,&04,&ee,&11,&fc,&f1,&cf,&0f
                        defb &0f,&0f,&cc,&67,&7f,&f0,&f0,&f0
                        defb &e6,&00,&fc,&80,&f0,&04,&f3,&80
                        defb &00,&07,&80,&00,&06,&11,&f8,&f3
                        defb &80,&00,&05,&33,&0f,&0f,&0f,&1f
                        defb &cc,&00,&fc,&f3,&8f,&0f,&0f,&0f
                        defb &cc,&67,&fe,&f0,&f0,&f0,&e6,&00
                        defb &fe,&80,&f0,&04,&f3,&80,&00,&07
                        defb &80,&00,&06,&11,&f8,&f3,&80,&00
                        defb &05,&33,&0f,&1f,&ff,&ff,&cc,&00
                        defb &fe,&f7,&80,&0f,&04,&cc,&77,&fe
                        defb &f0,&f0,&f0,&e6,&00,&ff,&ff,&fc
                        defb &f0,&f0,&f3,&80,&00,&07,&80,&00
                        defb &06,&11,&f8,&f3,&80,&00,&05,&33
                        defb &80,&ff,&04,&88,&00,&76,&ff,&80
                        defb &0f,&04,&cc,&77,&33,&f0,&f7,&ff
                        defb &ee,&00,&33,&ff,&ff,&ff,&fc,&f3
                        defb &80,&00,&07,&80,&00,&06,&11,&ff
                        defb &ff,&80,&00,&05,&11,&ff,&ee,&80
                        defb &00,&04,&77,&ef,&80,&0f,&04,&cc
                        defb &00,&33,&ff,&ff,&ee,&80,&00,&04
                        defb &77,&ff,&ff,&ee,&80,&00,&07,&80
                        defb &00,&06,&11,&ff,&ee,&80,&00,&0c
                        defb &33,&bb,&80,&ff,&04,&cc,&00,&33
                        defb &ff,&80,&00,&11,&80,&00,&06,&11
                        defb &ee,&80,&00,&0d,&11,&00,&33,&ff
                        defb &ff,&ff,&88,&80,&00,&14
