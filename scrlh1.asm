;; This example shows how to avoid the problem area when hardware scrolling horizontally.
;; We reduce the screen height AND we limit the range we scroll.
;;
;; We determine how many screens we want to scroll horizontally, we then adjust the height
;; by subtracting 1 from it for each screen's width.
;;
;; the max scroll_scrl_offset is &400-(R1*R6)
;;
;; Example:
;; if we want to scroll horizontally by up to 3 screens, we need to reduce the height by 3 lines.
;;
;; Use O on the keyboard to scroll left and P on the keyboard to scroll right
org &8000

num_screens equ 3
scr_height equ 25-num_screens
scr_width equ 40

max_scroll_scrl_offset equ &400-(scr_width*scr_height)

km_test_key equ &bb1e
km_read_key equ &bb1b
mc_wait_flyback equ &bd19
scr_set_mode equ &bc0e
txt_output	equ &bb5a

start:

;; set the screen mode
ld a,1
call scr_set_mode

;; fill screen with chars
ld bc,24*40
ld d,' '
l1:
inc d
ld a,d
cp &7f
jr nz,no_char_reset
ld d,' '
no_char_reset:
ld a,d
call txt_output
dec bc
ld a,b
or c
jr nz,l1

;; highlight problem area
ld a,&ff
ld (&c7ff),a
ld (&cfff),a
ld (&d7ff),a
ld (&dfff),a
ld (&e7ff),a
ld (&efff),a
ld (&f7ff),a
ld (&ffff),a

ld bc,&bc01
out (c),c
ld bc,&bd00+scr_width
out (c),c

;; set scr width
ld bc,&bc06
out (c),c
ld bc,&bd00+scr_height      ;; normal height is 25 chars, but we want to scroll 3 entire screens in width and avoid
;; the problem area.
out (c),c

loop:
;; wait for vsync 
;;
;; (synchronises scroll with the screen refresh; as a result it
;; updates at a constant rate and is smooth)
call mc_wait_flyback

call set_scroll

call check_keys

;; loop
jp loop


;;----------------------------------------------------------------------
check_keys:
ld a,34           ;; O
call km_test_key
jp nz,scroll_left
ld a,27          ;; P
call km_test_key
jp nz,scroll_right

ret

;;--------------------------------------------------------------------------------------------

scroll_left:
;; are we at the leftmost limit?
ld hl,(scroll_scrl_offset)
ld a,h
or l
ret z
ld hl,(scroll_scrl_offset)
;; no we can scroll
dec hl
ld (scroll_scrl_offset),hl
ret

;;--------------------------------------------------------------------------------------------

scroll_right:
;; are we at the rightmost limit?
ld hl,(scroll_scrl_offset)
ld bc,max_scroll_scrl_offset
or a
sbc hl,bc
ld a,h
or l
ret z
ld hl,(scroll_scrl_offset)
;; no, we can scroll
inc hl
ld (scroll_scrl_offset),hl
ret

;;--------------------------------------------------------------------------------------------
;; set scroll parameters in hardware
set_scroll:
ld hl,(scroll_scrl_offset)

;; write scroll scroll_scrl_offset (in CRTC character width units)
ld bc,&bc0c				;; select CRTC register 12
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write

;; combine with scroll base
ld a,(scroll_base)
or h
out (c),a

ld bc,&bc0d				;; select CRTC register 13
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write
out (c),l
ret

;;--------------------------------------------------------------------------------------------


;; high byte of the screen base address
;; &00 -> screen uses &0000-&3fff
;; &10 -> screen uses &4000-&7fff
;; &20 -> screen uses &8000-&bfff
;; &30 -> screen uses &c000-&ffff

scroll_base: 
defb &30

;; the scroll scroll_scrl_offset in CRTC character units
scroll_scrl_offset:
defw 0

end start