;; This example shows how to assign a string to a key.
;; 
;; e.g. when I press 'Q' the string "|DISC<CR>" is shown
;;
;; NOTE: 
km_set_expand equ &bb0f
km_char_return equ &bb0c
km_exp_buffer equ &bb15
km_set_translate equ &bb27

org &4000
nolist

;; the expansion buffer initialisation is not generally
;; needed.
;; 
;; But this shows how to do it
;;
;;ld de,exp_buffer
;;ld hl,end_exp_buffer-exp_buffer
;;call km_exp_buffer
;;jp nc,0


;; Define a string for a specific expansion token
;; Tokens are numbered &80-&fc
;;
ld b,&80			;; &80
ld c,end_cmd_string-cmd_string
ld hl,cmd_string
call km_set_expand
jp nc,0000

;; now associated key Q with token &80
ld a,&51						;; key number for Q
ld b,&80					;; token it should generate
call km_set_translate

;; NOTE: If you wanted to autotype a command, then you would
;; think you could then put the ASCII code of the char into the
;; keyboard buffer using KM CHAR RETURN. However, when you do this
;; the char is not expanded or translated. So this doesn't work.
ret

cmd_string:
defb "|DISC",13
end_cmd_string:

;;exp_buffer:
;;defs 2048
;;end_exp_buffer: