;; plot a pixel using the firmware graphics functions
;;
;; This code is mode independant.
;;
;; NOTE: That for mode 2 (because it is only 2 colours) the pen must be changed.

gra_plot_absolute equ &bbea
gra_set_pen equ &bbde
scr_set_mode equ &bc0e
kl_u_rom_enable equ &b900
txt_set_paper equ &bb96
txt_clear_window equ &bb6c

org &4000

;; uncomment to see that this causes no effect
;;call kl_u_rom_enable


;; set mode 0 and clear to paper
xor a
call scr_set_mode	


;; set paper
ld a,1
call txt_set_paper

;; clears window to paper colour
call txt_clear_window

;; set pen 3
ld a,3				
call gra_set_pen	

;; plot point
;; this uses graphics coordinates
;; x coord (in range 0-639)
;; y coord (in range 0-400)
;; origin is bottom left

ld de,300		;; x
ld hl,100		;; y
call gra_plot_absolute
ret
