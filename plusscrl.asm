org &8000
nolist
;;write direct "plusscrl.bin"
;;run start

scr_set_mode equ &bc0e
txt_output equ &bb5a

start:
ld a,2
call scr_set_mode
ld bc,24*80
ld d,' '
l1:
inc d
ld a,d
cp &7f
jr nz,no_char_reset
ld d,' '
no_char_reset:
ld a,d
call txt_output
dec bc
ld a,b
or c
jr nz,l1

di
ld b,&bc
ld hl,sequence
ld e,17
seq:
ld a,(hl)
out (c),a
inc hl
dec e
jr nz,seq

ld hl,&c9fb
ld (&0038),hl
ei

ld bc,&7fb8
out (c),c

ld a,8
ld (&6800),a

mainloop:

;; scroll down
ld a,(vscroll1)
add a,16
and %01110000
ld (vscroll1),a

;; scroll up
ld a,(vscroll2)
sub 16
and %01110000
ld (vscroll2),a

;; scroll right
ld a,(hscroll1)
inc a
and %1111
ld (hscroll1),a

;; scroll left
ld a,(hscroll2)
dec a
and %1111
ld (hscroll2),a

;; scroll down
ld a,(vscroll3)
add a,16
and %01110000
or %10000000
ld (vscroll3),a

;; scroll up
ld a,(vscroll4)
sub 16
and %01110000
or %10000000
ld (vscroll4),a

;; scroll right
ld a,(hscroll3)
inc a
and %1111
or %10000000
ld (hscroll3),a

;; scroll left
ld a,(hscroll4)
dec a
and %1111
or %10000000
ld (hscroll4),a

halt

ld hl,&6804			;; [3]
ld de,(vscroll)		;; [6]

call set_scroll

ld a,58
ld (&6800),a

halt

ld hl,&6804			;; [3]
ld de,(vscrollb)		;; [6]

call set_scroll

ld a,106
ld (&6800),a

halt

ld hl,&6804
ld de,(hscroll)

call set_scroll

ld a,154
ld (&6800),a

halt

ld hl,&6804
ld de,(hscrollb)

call set_scroll

ld a,8
ld (&6800),a

jp mainloop

set_scroll:
;; 5 here
xor a				;; [1]
defs 54

;; no scroll initially
ld (hl),a			;; [2]

ld c,32				;; [2]
set_scroll2:
defs 4
ld (hl),D			;; [2]
defs 8
;; middle non-scrolling section
ld (hl),a			;; [2]
defs 6
ld (hl),e			;; [2]
defs 8
ld (hl),a			;; [2]
defs 26	
dec c				;; [1]
jp nz,set_scroll2	;; [3]	

;; and end with no scroll
ld (hl),a
ret


vscroll:
vscroll1:
defb 0
vscroll2:
defb 0

vscrollb:
vscroll3:
defb 0
vscroll4:
defb 0

hscroll:
hscroll1:
defb 0
hscroll2:
defb 0

hscrollb:
hscroll3:
defb 0
hscroll4:
defb 0

sequence:
defb &ff,&00,&ff,&77,&b3,&51,&a8,&d4,&62,&39,&9c,&46,&2b,&15,&8a,&cd,&ee

;;end start