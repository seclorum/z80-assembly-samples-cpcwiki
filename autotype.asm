
;;--------------------------------------------------------------------------------------------

kl_new_frame_fly equ &bcd7
kl_del_frame_fly equ &bcdd
km_char_return equ &bb0c

;;--------------------------------------------------------------------------------------------

org	&8000

ld b,end_cmd_string-cmd_string
ld hl,cmd_string
call autotype_string
ret

cmd_string:
defb "|DISC",13
end_cmd_string:

str_ptr:
defw 0

str_remaining:
defb 0

autotype_string:
ld (str_ptr),hl
ld a,b
ld (str_remaining),a

;; setup an interrupt to be executed every frame flyback 
;; (50hz)
ld hl,ff_event_block
ld b,&81
ld c,0
ld de,ff_event_routine
call kl_new_frame_fly
ret

;;--------------------------------------------------------------------------------------------

stop_autotype:
ld hl,ff_event_block
call kl_del_frame_fly
ret

;;--------------------------------------------------------------------------------------------

ff_event_block:
defs 10

;;--------------------------------------------------------------------------------------------

ff_event_routine:

;; location char is returned to.
ld a,(&b62a)
cp &ff
ret nz

ld a,(str_remaining)
or a
jp z,stop_autotype
dec a
ld (str_remaining),a
ld hl,(str_ptr)
ld a,(hl)
inc hl
ld (str_ptr),hl

call km_char_return
ret


