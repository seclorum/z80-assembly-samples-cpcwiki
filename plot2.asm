;; plot a pixel using the firmware screen functions
;;

scr_set_mode equ &bc0e
scr_dot_position equ &bc1d
scr_ink_encode equ &bc2c
scr_pixels equ &bc5c
kl_u_rom_enable equ &b900
txt_set_paper equ &bb96
txt_clear_window equ &bb6c

org &4000

;; uncomment to see that this causes no effect
;;call kl_u_rom_enable

;; set mode 0
xor a
call scr_set_mode	

;; set paper
ld a,1
call txt_set_paper

;; clears window to paper colour
call txt_clear_window

;; plot point
;;
;; for mode 0: x is in the range 0-159
;; for mode 1: x is in the range 0-320
;; for mode 2: x is in the range 0-639
;; for all modes: y is in the range 0-200
;; origin is bottom left

;; get address of pixel
;; change coords for mode 1 and 2 to get same location as mode 0
ld de,80		;; x 
ld hl,100		;; y 
call scr_dot_position
;; HL = address of byte containing pixel
;; C = bit mask for pixel
;; B = number of pixels in byte

push hl
push bc

;; set pen; change for mode 2
ld a,3				;; pen 3
call scr_ink_encode
;; A = pen 3 encoded into a form to poke to screen
;; but which would set all pixels in that byte

pop bc
pop hl

ld b,a
;; C = mask from above

;; write to screen
call scr_pixels

ret
