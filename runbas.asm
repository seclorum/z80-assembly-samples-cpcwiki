
;; BASIC RUNNING TAKES DIFFERENT APPROACH
;; must set start/end of basic prog then set vars area then jump to prog

let total_length=bas_end-bas_run

.basic_run
ld hl,end_rom
ld de,&a000
ld bc,total_length
ldir
jp &a000
.end_rom

let rom_tlength=end_rom-start_rom
let sub_addr=rom_tlength+old_addr
list
org &a000,sub_addr
nolist
.bas_run
ld hl,(length)
ld bc,&170
add hl,bc
ld c,0
call kl_rom_select
ld a,(&c002)
cp 0
jp z,cpc464
ld (&ae66),hl
ld (&ae68),hl
ld (&ae6a),hl
ld (&ae6c),hl
cp 1
jp z,cpc664
jp &ea78        ;; run BASIC CPC6128
.cpc664
jp &ea7d        ;; run BASIC CPC664
.cpc464
ld (&ae83),hl
ld (&ae85),hl
ld (&ae87),hl
ld (&ae89),hl
jp &e9bd        ;; run BASIC CPC464
.bas_end
list
end
