	org &4000

.inkey	equ &bb09
.mode	equ &bc0e
.inks	equ &bc32
.border equ &bc38
.print	equ &bb5a
.matrix equ &bba8
.plot	equ &bbea
.pen	equ &bb90
.grapen equ &bbde
.scroll	equ &bc4d
.locate	equ &bb75
.frame	equ &bd19
.colres	equ &bc02

	call setup
	ld a,(xpos)
	ld h,a
	ld l,25
	call locate	
	ld a,254
	call print
	ld a,255
	call print
	ld hl,direction
	ld a,&3d
	ld (hl),a
.mainloop

	call inkey
	cp &0d
	jr z,exit
	ld a,(xpos)
.direction	
	defb 0
	ld (xpos),a
	ld h,a
	ld l,25
	call locate

	call frame

	ld a,254
	call print
	ld a,255
	call print
	
	call frame

	ld a,(xpos)
	cp &1
	call z,change_value1
	
	ld a,(xpos)
	cp &13
	call z,change_value2

	ld hl,divider
	ld a,3
	ld (hl),a

	call rand8
	
	inc a
	ld (result),a

	ld a,(result)
	call grapen
	
	ld hl,divider
	ld a,255
	ld (hl),a

	call rand8

	ld (result),a

	ld hl,divider
	ld a,7
	ld (hl),a

	call rand8

	ld (result+1),a

	ld de,(result)
	ld hl,398
	call plot	

	ld b,0
	ld a,0
	call scroll

	jp mainloop

.exit	ret	
	
.setup	xor a
	call mode
	ld a,4
	call pen
	ld bc,0
	call border
	ld hl,colours
	ld a,0
.setup_inks
	ld c,(hl)
	ld b,c
	push af
	push hl
	call inks
	pop hl
	pop af
	inc hl
	inc a
	cp &5
	jr c,setup_inks
	ld a,254
	ld hl,pattern1
	call matrix
	ld a,255
	ld hl,pattern2
	call matrix	
	ret

.change_value1
	ld hl,direction
	ld a,&3c
	ld (hl),a
	ret

.change_value2
	ld hl,direction
	ld a,&3d
	ld (hl),a
	ret

.rand8	ld a,(seed)
	ld b,a
	add a,a
	add a,a
	add a,b
	inc a
	ld (seed),a
	ld a,(divider)
	ld c,a
	ld a,0
	ld hl,(seed)
	ld b,16
.divide	add hl,hl
	rla
	cp c
	jr c,end
	sub c
	inc l
.end	djnz divide
	ret

.randseed
	defw 7			;; random seed, any old number will do
.colours
	defb 0,26,26,13,6
.pattern1
	defb 1,1,65,67,71,127,67,1
.pattern2
	defb 128,128,130,194,226,254,194,128
.xpos	defb 10
.seed	defb 7
.result	defb 0,0
.divider
	defb 3