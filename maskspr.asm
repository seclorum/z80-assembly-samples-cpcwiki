;; Example to draw a masked sprite to the screen
;;
;; The sprite pixels are stored in a form that can be poked
;; direct to screen and seen.
;;
;; This example uses mode 0 sprites where pen 0 is transparent
;; leaving 15 other pens for sprites.
;;
;; A mask table/array is generated for each byte combination (mask table
;; is therefore 256 bytes long).
;;
;; Using the sprite pixel data, the appropiate mask is found in the
;; table/array.
;; 
;; The sprite is then combine with the screen pixels to the display.
;;
;; However, drawing the sprite to the display also changes it.
;; So in this example the rectangle that the sprite will cover
;; is stored into a buffer so it can be copied back to the screen when
;; the sprite is moved.
;;
;; This source released under the GNU Library License v2 or later.
;; Code by Kev Thacker 2010
;;
;; NOTE: Sprite will disapear as you move up the screen this is
;; because the sprite background has been drawn and then by the time the
;; code comes to draw the sprite the display refresh has already passed, so it 
;; doesn't show the update.

sprite_width equ 12/2
sprite_height equ 21

;; firmware functions we use
scr_next_line equ &bc26
scr_set_mode equ &bc0e
scr_set_border equ &bc38
scr_set_ink equ &bc32
mc_wait_flyback equ &bd19
km_read_char equ &bb09

;; table is aligned on a 256-byte boundary
mask_table equ &200

org &1000
nolist

start:
;; set 16 colour mode 0
xor a
call scr_set_mode

;; set border to black
ld bc,0
call scr_set_border

;; set pens
ld hl,pens
ld b,16			;; 16 pens for mode 0
xor a			;; initial pen index
set_pens
push bc
push af
ld c,(hl)		;; B,C = inks for pen. If they are the same then no 
				;; flashing
ld b,c
inc hl
push hl
call scr_set_ink
pop hl
pop af
pop bc
inc a			;; increment pen index
djnz set_pens

ld hl,scr_pixels
ld de,&c000
ld bc,&4000
ldir



call make_scr_table
call init_mask_table

;; define initial sprite coords
ld h,50
ld l,150
ld (sprite_coords),hl

;; store background pixels where sprite is located
ld hl,(sprite_coords)
ld de,sprite_background
ld b,sprite_height
ld c,sprite_width
call sprite_background_store

;; draw sprite (writes over background pixels)
ld de,sprite_pixels
ld hl,(sprite_coords)
ld b,sprite_height
ld c,sprite_width
call sprite_draw

;; the main loop which draws and updates sprite
main_loop:
call mc_wait_flyback

;; restore background (where sprite used to be)
ld hl,(sprite_coords)
ld de,sprite_background
ld b,sprite_height
ld c,sprite_width
call sprite_background_restore

;; check keys and update sprite position
call check_keys


;; store background pixels where sprite is now located
ld hl,(sprite_coords)
ld de,sprite_background
ld b,sprite_height
ld c,sprite_width
call sprite_background_store

;; draw sprite in new location
ld de,sprite_pixels
ld hl,(sprite_coords)
ld b,sprite_height
ld c,sprite_width
call sprite_draw

jp main_loop


;; This function will generate a lookup table of pixel masks.
;; This table can be used to plot masked sprites onto a mode 0 display.
;; 
;;
;; call init_mask_table to initialise.
;; 
;; 
;; NOTES:
;; - This code is for mode 0.
;; - The pen which will be plotted transparent is 0.
;; - Relocate mask_table to an address which is on a 256 byte boundary. (i.e. 
;; it's low byte is 0). This will allow you to access it much quicker.

.init_mask_table
ld hl,mask_table						;; base address of the mask table
ld b,0							;; counter
ld c,0							;; initial pixel value

mmt1:
ld d,0							;; initialise initial mask

ld a,c
and &aa							;; isolate bits used to define the pen for the left pixel.
jr z,mmt2

;; the pixel is transparent

;; update mask so that it will keep the left pixel from the 
;; screen's byte
ld a,d
or &aa
ld d,a

mmt2:
ld a,c
and &55							;; isolate bits used to define the pen for the left pixel.
jr z,mmt3

;; the pixel is transparent

;; update mask so that it will keep the right pixel from the 
;; screen's byte
ld a,d
or &55
ld d,a

mmt3:
ld a,d
cpl 
ld (hl),a						;; store final mask in table
inc hl							;; increment position in table
inc c							;; increment pixel value
djnz mmt1						;; loop
ret


;; check if a key has been pressed and perform action if it has
check_keys:
call km_read_char
ret nc
;; A = ascii char of key pressed
;; we check for both upper and lower case chars
cp 'q'
jp z,sprite_up
cp 'Q'
jp z,sprite_up
cp 'a'
jp z,sprite_down
cp 'A'
jp z,sprite_down
cp 'o'
jp z,sprite_left
cp 'O'
jp z,sprite_left
cp 'p'
jp z,sprite_right
cp 'P'
jp z,sprite_right
ret

;; move sprite one byte to left
;; 2 pixels in mode 0
sprite_left:
ld a,(sprite_x)
or a
ret z
dec a
ld (sprite_x),a
ret

;; mode sprite one byte to right
;; 2 pixels in mode 0
right_side equ 80-sprite_width

sprite_right:
ld a,(sprite_x)
cp right_side
ret z
inc a
ld (sprite_x),a
ret

;; move sprite up one scan line
sprite_up:
ld a,(sprite_y)
or a
ret z
dec a
ld (sprite_y),a
ret

bottom_side equ 199-sprite_height

;; move sprite down one scan line
sprite_down:
ld a,(sprite_y)
cp bottom_side
ret z
inc a
ld (sprite_y),a
ret

;; sprite coordinates 
;; these are updated
sprite_coords:
;; Y line coordinate of sprite
sprite_y:
defb 0

;; x byte coordinate of sprite
sprite_x:			
defb 0

;; From our x,y coordinates generate
;; a screen address for top-left of sprite to be drawn at.

;; IN:
;; H = x byte coord
;; L = y line
;; OUT:
;; HL = screen address
get_scr_addr:
push bc
push de
ld c,h
ld h,0
add hl,hl
ld de,scr_addr_table
add hl,de
ld a,(hl)
inc hl
ld h,(hl)
ld l,a
ld b,0
add hl,bc
pop de
pop bc
ret

;; generate table of screen addresses
;; one address per scanline. The address
;; is for the first byte of each line.
make_scr_table:
ld ix,scr_addr_table		;; address to store table
ld hl,&c000					;; start address of first scanline
ld b,200					;; number of scanlines on screen
mst1:
ld (ix+0),l
ld (ix+1),h
inc ix
inc ix
push bc
call scr_next_line
pop bc
djnz mst1
ret

;; draw a masked sprite to the screen
;;
;; H = x byte coord
;; L = y line
;; DE = current memory address of sprite pixel data
;; B = height
;; C = width
;;
;; data for sprite:
;;
;; Each byte is a byte of sprite pixel data,
;; no mask is stored.

sprite_draw:
call get_scr_addr

push bc
pop ix							;; low byte of IX = C
								;; high byte of IX = B
								
;; B = upper 8-bits (bits 15..8) of mask table memory address
ld b,mask_table/256

sprite_draw_height:
push ix
push hl


sprite_draw_width:
ld a,(de)							;; get byte of sprite pixel data
ld c,a								;; C = byte of sprite pixel data/look-up table value
									;; BC = address (in look-up table) of mask corresponding to this sprite pixel data
ld a,(bc)							;; lookup mask from table
and (hl)							;; mask pixels on screen (remove pixels which will be replaced)
or c								;; combine with sprite pixel data
ld (hl),a							;; write result to screen 
inc de
inc hl
defb &dd							;; DEC LIX
dec l
jr nz,sprite_draw_width
pop hl
call scr_next_line
pop ix
defb &dd							;; DEC HIX
dec h
jr nz,sprite_draw_height
ret

;; H = x byte coord
;; L = y line
;; DE = address to store screen data
;; B = height
;; C = width
;; store a rectangle from the screen into a buffer
sprite_background_store:
call get_scr_addr

sprite_back_height:
push bc
push hl

sprite_back_width:
ld a,(hl)				;; read from screen
ld (de),a				;; store to buffer
inc hl
inc de
dec c
jr nz,sprite_back_width

pop hl
call scr_next_line
pop bc
djnz sprite_back_height
ret

;; H = x byte coord
;; L = y line
;; DE = address to store screen data
;; B = height
;; C = width
;;
;; restore a rectangle to the screen
sprite_background_restore:
call get_scr_addr

sprite_reback_height:
push bc
push hl

sprite_reback_width:
ld a,(de)					;; read from buffer
ld (hl),a					;; write to screen
inc hl
inc de
dec c
jr nz,sprite_reback_width

pop hl
call scr_next_line
pop bc
djnz sprite_reback_height
ret

;; a buffer to store screen behind sprite
sprite_background:
defs sprite_height*sprite_width

;; table/array for screen addresses for each scan line
scr_addr_table:
defs 200*2

;; the inks for the pens
pens:
defb &00
defb &16
defb &0b
defb &09
defb &10
defb &01
defb &17
defb &0a
defb &11
defb &19
defb &1a
defb &03
defb &0e
defb &0c
defb &04
defb &0d

scr_pixels:
.scr
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&39,&36,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &33,&33,&33,&33,&3c,&3c,&3c,&3c,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&0c,&33,&33,&33,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&19,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &36,&3c,&3c,&3c,&36,&33,&36,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&fc,&be,&33,&1e,&3c,&3c,&fe,&be 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &44,&00,&00,&f3,&ff,&ff,&ff,&ff,&fb,&aa,&fb,&a2,&aa,&e6,&ce,&f7 
defb &f7,&ff,&fb,&fb,&ff,&ff,&af,&ff,&fb,&25,&85,&af,&0f,&0f,&0f,&1a 
defb &0f,&0f,&0f,&0f,&87,&87,&5f,&4b,&ba,&ff,&af,&5f,&bb,&27,&1b,&ff 
defb &ff,&0b,&af,&4b,&87,&0f,&4b,&0f,&0f,&0f,&87,&5f,&ff,&be,&bf,&ff 
defb &ff,&ff,&f4,&fc,&f4,&fc,&f4,&f4,&fc,&fc,&fc,&fc,&f0,&fc,&fc,&fc 
defb &f3,&d9,&f3,&a2,&f3,&d9,&f3,&f3,&f3,&a2,&fc,&a0,&f8,&a8,&f8,&f0 
defb &f8,&00,&f0,&f0,&a0,&00,&00,&00,&00,&00,&50,&f0,&50,&50,&00,&f0 
defb &f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &8a,&f3,&a2,&a2,&a2,&d9,&f3,&f3,&44,&e6,&88,&44,&a2,&f3,&ff,&ff 
defb &fd,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&f0,&fc,&f0,&f8,&f8,&fc,&fc,&fc,&f4,&fc,&fc,&f0,&f4,&f8 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &dd,&f7,&f3,&f3,&e6,&f3,&d9,&88,&e6,&44,&00,&00,&44,&44,&cc,&cc 
defb &51,&ff,&ff,&dd,&ff,&ff,&ff,&ff,&7d,&ff,&fe,&fe,&fd,&5f,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f0,&fc,&5c 
defb &f0,&f8,&f8,&f0,&f0,&f0,&f4,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &fc,&f8,&f8,&f0,&f0,&f0,&f8,&f8,&f0,&fc,&f8,&f0,&f0,&f0,&f0,&f0 
defb &d9,&cc,&88,&cc,&51,&00,&f3,&e6,&cc,&cc,&cc,&44,&cc,&44,&f3,&e6 
defb &aa,&cc,&c8,&5f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&fe,&fc,&ff,&fe,&fc,&fc,&fc,&0c,&fc,&fc,&fc,&fc,&fe 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f0,&f8,&f0,&fc,&f0,&f4 
defb &f4,&f4,&fc,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&e6,&88,&88,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &44,&00,&00,&51,&ee,&f3,&f7,&be,&1e,&39,&27,&ff,&c0,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fd,&ff,&bb 
defb &fd,&fe,&fc,&ec,&df,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&fc,&f4,&fc,&f8,&f0,&f0,&f0 
defb &44,&00,&00,&f3,&55,&f3,&e6,&f3,&d9,&00,&44,&00,&00,&51,&f3,&cc 
defb &a2,&00,&d9,&00,&00,&a2,&f3,&f3,&f3,&d9,&fb,&af,&0f,&1e,&3c,&2d 
defb &77,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fe,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&ac,&0c,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &44,&88,&44,&d9,&d9,&88,&00,&51,&a2,&cc,&cc,&00,&88,&88,&44,&44 
defb &00,&cc,&00,&44,&e6,&88,&44,&e6,&cc,&cc,&cc,&e6,&51,&eb,&4b,&0b 
defb &0f,&0f,&0f,&1e,&0f,&0f,&4b,&30,&c3,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&fe,&fc,&fc,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &ee,&ff,&a8,&00,&51,&88,&00,&00,&44,&88,&44,&51,&51,&00,&00,&00 
defb &00,&00,&00,&00,&00,&88,&00,&aa,&88,&00,&00,&00,&00,&88,&a7,&87 
defb &0f,&0f,&0f,&0f,&0f,&0f,&2d,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &fb,&f7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fd,&ff,&fc,&fc,&ec 
defb &ff,&ff,&00,&00,&00,&00,&00,&00,&a2,&44,&88,&00,&00,&00,&00,&44 
defb &00,&00,&44,&00,&00,&00,&00,&e6,&00,&cc,&00,&00,&88,&44,&d9,&ba 
defb &30,&b2,&30,&34,&25,&4b,&87,&4b,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&75,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&a2,&75,&d9 
defb &00,&ff,&ff,&00,&00,&dc,&88,&00,&00,&00,&00,&51,&00,&88,&00,&44 
defb &cc,&fb,&44,&44,&00,&00,&00,&00,&e6,&00,&00,&00,&a2,&87,&0f,&0f 
defb &0f,&0f,&4b,&d7,&af,&c3,&0f,&0f,&0f,&0f,&0f,&1a,&87,&0f,&0f,&4b 
defb &0f,&0b,&0f,&0f,&0f,&0f,&0f,&0f,&0b,&0b,&0f,&0f,&c3,&92,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fb,&aa,&b8,&ff 
defb &45,&00,&10,&ff,&aa,&55,&ff,&ec,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&44,&00,&51,&00,&44,&d9,&aa,&00,&e7,&e3,&eb,&aa 
defb &75,&a7,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&0f,&f3,&51,&eb,&0f,&0f 
defb &5f,&55,&0f,&0f,&0f,&d7,&d7,&c3,&87,&af,&ea,&0f,&3c,&4b,&57,&33 
defb &c3,&87,&87,&0f,&5f,&d7,&ff,&ff,&ff,&ff,&fb,&ff,&ff,&ff,&ff,&ff 
defb &51,&00,&00,&00,&ff,&ff,&ff,&88,&00,&00,&00,&51,&00,&00,&00,&aa 
defb &00,&00,&00,&00,&44,&00,&ee,&51,&88,&f7,&ff,&00,&51,&00,&ee,&51 
defb &d9,&a2,&00,&45,&aa,&eb,&5f,&d3,&51,&fb,&00,&e6,&f3,&a2,&44,&00 
defb &ff,&c3,&e6,&f7,&aa,&00,&00,&51,&0a,&d9,&88,&f3,&55,&00,&aa,&aa 
defb &44,&dd,&44,&cc,&44,&e6,&f3,&ff,&eb,&aa,&e6,&44,&ff,&fb,&aa,&f3 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&33,&3c,&39,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&19,&33,&33,&33,&33,&26,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &36,&3c,&3c,&3c,&fe,&ff,&be,&3c,&36,&7d,&36,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&50,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&dc,&a8,&fd,&fd,&ec,&44,&ec 
defb &fc,&bc,&39,&ff,&ff,&3c,&39,&36,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&36,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &44,&00,&44,&dd,&af,&75,&af,&5f,&af,&87,&5f,&5f,&5f,&0f,&61,&0f 
defb &96,&87,&0f,&0f,&0f,&1e,&3c,&ea,&33,&bb,&77,&77,&be,&76,&bb,&fe 
defb &ff,&fd,&76,&fc,&ff,&fc,&fc,&fc,&fd,&b9,&fc,&76,&fe,&fd,&7d,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fe,&77,&fc,&fc,&fc,&fc 
defb &fc,&fe,&fc,&f0,&f4,&f0,&f0,&f0,&f0,&f4,&fc,&fc,&fc,&fc,&fc,&fc 
defb &f3,&f3,&f3,&f3,&f3,&f3,&f3,&f3,&f3,&f3,&ff,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&f4,&fc,&f0,&f8,&f0,&f8,&f0,&f0,&f0,&50,&f0,&00,&f4 
defb &f8,&f0,&f4,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&50,&fd,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &45,&f3,&f3,&a2,&a2,&e6,&a2,&f3,&00,&d9,&88,&d9,&a2,&f3,&e6,&dd 
defb &ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&f8,&f0,&fc,&f0,&f0,&f8,&f0,&f0,&f8,&f8,&fc,&fc,&f8,&f0,&f4 
defb &f8,&f4,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f7,&fb,&fb,&f3,&f3,&f7,&e6,&f3,&f3,&f3,&88,&44,&00,&44,&00,&cc 
defb &f3,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fd,&fc,&fe,&fd,&fe,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&fc 
defb &fc,&fc,&fc,&f8,&f4,&fc,&f4,&f0,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f4 
defb &f4,&f8,&fc,&f0,&f0,&f4,&fc,&fc,&f0,&f0,&f4,&fc,&f0,&f0,&f8,&f0 
defb &cc,&dd,&88,&88,&51,&cc,&44,&cc,&d9,&00,&00,&a2,&d9,&51,&f3,&f3 
defb &00,&cc,&dd,&ff,&c3,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&fe,&fc,&fc,&fc,&fc,&bb,&33,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f4,&fc,&fc,&fc,&fc,&f0,&f0 
defb &f4,&fc,&f4,&f4,&f0,&f0,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &cc,&cc,&dd,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&44 
defb &cc,&cc,&cc,&55,&fb,&f3,&f3,&94,&3c,&2d,&7d,&5f,&c0,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&77,&ff,&ff,&fe 
defb &ff,&fc,&fc,&fd,&55,&dc,&ec,&cc,&dc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&f0,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&f8 
defb &44,&88,&44,&51,&e6,&cc,&44,&f3,&e6,&00,&00,&00,&00,&e7,&d9,&f3 
defb &f3,&00,&e6,&00,&00,&00,&e6,&e6,&e6,&00,&fb,&bb,&0f,&3c,&0f,&3c 
defb &3c,&5f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&ff,&fe,&fd 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fd,&fe,&fc,&fc,&fc,&fc,&fc 
defb &44,&00,&44,&00,&cc,&88,&51,&f3,&51,&44,&44,&00,&a2,&51,&cc,&00 
defb &51,&e6,&88,&44,&e6,&00,&cc,&cc,&a2,&88,&cc,&fb,&44,&05,&c3,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&c3,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &ee,&ff,&00,&00,&00,&44,&00,&00,&88,&00,&00,&44,&44,&00,&44,&00 
defb &00,&00,&00,&00,&cc,&00,&00,&a2,&88,&00,&00,&88,&44,&e6,&ba,&25 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&1e,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&c0,&d5,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fe,&fc 
defb &ff,&ff,&00,&00,&00,&00,&00,&00,&e6,&00,&00,&00,&88,&00,&44,&00 
defb &00,&55,&44,&88,&00,&00,&00,&aa,&44,&ee,&00,&44,&44,&44,&d9,&e7 
defb &9a,&b2,&92,&d7,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&92,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&8a,&f7,&fd 
defb &00,&df,&ff,&a8,&00,&87,&68,&00,&00,&00,&00,&cc,&00,&00,&00,&51 
defb &dd,&cc,&00,&44,&88,&00,&00,&88,&44,&44,&00,&00,&55,&af,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&ff,&0f,&0f,&0f,&0f,&0f,&87,&0f,&87,&0f,&0f 
defb &87,&0f,&5f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&af,&0f,&4b,&c3,&75,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fb,&aa,&41,&ff 
defb &00,&00,&55,&ff,&aa,&55,&ff,&a8,&00,&00,&00,&00,&00,&00,&00,&00 
defb &44,&00,&00,&00,&44,&00,&00,&88,&00,&88,&00,&55,&fb,&eb,&eb,&5b 
defb &f7,&af,&af,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&d9,&55,&eb,&0f,&0f 
defb &d7,&af,&0f,&0f,&0f,&20,&1a,&c3,&c3,&eb,&ea,&4b,&0f,&5f,&ff,&eb 
defb &eb,&87,&af,&5f,&5f,&c3,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&f7,&ff,&ff 
defb &00,&8a,&00,&00,&ff,&ff,&ff,&aa,&00,&00,&00,&55,&00,&00,&00,&8a 
defb &a2,&88,&00,&00,&88,&00,&ee,&44,&f3,&fb,&87,&a2,&f3,&00,&a2,&44 
defb &88,&88,&00,&aa,&a2,&f3,&a2,&f3,&51,&fb,&00,&e6,&a2,&00,&00,&00 
defb &e7,&ff,&f3,&a7,&aa,&00,&00,&85,&5b,&aa,&cc,&e6,&44,&e6,&55,&aa 
defb &88,&cc,&44,&44,&e6,&44,&00,&55,&eb,&aa,&cc,&51,&cc,&ff,&a2,&51 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&33,&33,&33,&33,&33,&26,&0c,&19 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&39 
defb &3c,&39,&36,&3c,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &b9,&3c,&3c,&3c,&aa,&00,&dc,&3c,&fc,&fe,&dc,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&51,&00,&00,&00,&8a,&00 
defb &88,&55,&aa,&00,&00,&54,&be,&3c,&3c,&36,&39,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&fe,&00,&55,&ff,&ff,&fc 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &88,&44,&44,&d9,&30,&eb,&0f,&0f,&ba,&0f,&0f,&0f,&0f,&0f,&4a,&85 
defb &94,&ff,&ff,&fe,&f8,&f8,&fe,&fc,&f4,&fc,&f8,&f0,&f8,&f8,&f4,&f4 
defb &f0,&f8,&78,&f8,&f8,&f0,&f0,&f0,&f4,&f8,&fc,&f0,&72,&a0,&fe,&f0 
defb &f0,&b9,&fa,&f0,&f0,&f0,&f0,&f0,&f8,&f4,&72,&fe,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f4,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f3,&f7,&fb,&f3,&f3,&f3,&f3,&f3,&f3,&dd,&fe,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f4,&fc,&fc,&fc 
defb &fc,&fc,&f8,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f7,&f3,&f3,&f3,&51,&d9,&a2,&fb,&f3,&f3,&cc,&88,&a2,&51,&f3,&e6 
defb &ee,&f7,&ff,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f8,&f0,&fc,&fc,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&fc,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &ff,&ff,&fb,&f3,&f7,&f3,&d9,&f3,&f3,&f7,&e6,&00,&00,&44,&00,&e6 
defb &55,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fd,&fe,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&fc 
defb &fc,&f4,&fc,&fc,&fc,&f0,&f0,&f0,&f0,&f0,&fc,&f0,&f4,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f8,&f0,&f4,&fc,&f4,&f0,&f0,&f0,&f0,&f8,&f0,&f8,&f0 
defb &cc,&88,&00,&00,&00,&cc,&00,&00,&88,&51,&cc,&88,&44,&cc,&51,&51 
defb &f3,&cc,&ee,&f3,&eb,&75,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&77,&fc,&fc,&fe,&fe,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f0,&f4,&fc 
defb &fc,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &cc,&00,&88,&00,&00,&00,&00,&00,&00,&00,&00,&44,&00,&d9,&cc,&f3 
defb &e6,&d9,&e6,&51,&fb,&f3,&ff,&ff,&1e,&2d,&3c,&0f,&c3,&d7,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&33,&ff,&fe,&fc 
defb &ff,&ff,&fe,&fc,&55,&fc,&fc,&fc,&dc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f0,&f0,&fc,&fc,&fc,&fc,&f8 
defb &00,&44,&cc,&51,&e6,&a2,&55,&cc,&f3,&a2,&d9,&a2,&00,&51,&51,&f3 
defb &d9,&e6,&88,&88,&d9,&51,&e6,&d9,&cc,&00,&cc,&ea,&1e,&2d,&3c,&0f 
defb &0f,&0f,&92,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&cc,&cc,&fc,&fc,&fc,&cc,&fc,&fc,&fc,&fc 
defb &88,&00,&88,&00,&88,&e6,&44,&44,&51,&51,&00,&88,&cc,&44,&a2,&44 
defb &d9,&cc,&ee,&88,&00,&00,&88,&00,&88,&00,&cc,&e6,&cc,&af,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&eb,&87,&d7,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&fd,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &88,&ff,&00,&00,&00,&44,&44,&00,&00,&00,&00,&00,&88,&00,&44,&88 
defb &00,&00,&00,&00,&00,&00,&00,&88,&cc,&00,&00,&00,&51,&a2,&b2,&25 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&27,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc,&ff,&fc 
defb &ff,&ff,&00,&00,&00,&00,&00,&00,&44,&a2,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&88,&00,&00,&00,&44,&a2,&00,&00,&51,&00,&51,&ba,&e7 
defb &e7,&9a,&30,&25,&0f,&0f,&0f,&0f,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&c3,&d7,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&8a,&9a,&fd 
defb &00,&55,&ff,&ee,&00,&af,&0f,&00,&00,&00,&00,&00,&51,&00,&00,&00 
defb &88,&51,&00,&00,&88,&00,&00,&00,&44,&44,&d9,&e6,&00,&af,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&0f,&0f,&1a,&87,&0f,&0f,&0f 
defb &c3,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&af,&af,&0f,&4b,&92,&30,&c3 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ef,&fb,&ff 
defb &00,&00,&45,&ff,&ff,&55,&ff,&aa,&00,&00,&44,&00,&cc,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&88,&55,&f7,&ee,&a2,&b2,&c2 
defb &ba,&af,&87,&25,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&44,&40,&55,&0f,&eb 
defb &dd,&0f,&0f,&0f,&4b,&4f,&75,&4b,&d7,&eb,&77,&eb,&5f,&fb,&f7,&85 
defb &55,&92,&25,&d7,&af,&0f,&ff,&ff,&ff,&ff,&ff,&f7,&f7,&ff,&ff,&ff 
defb &00,&00,&a2,&00,&ff,&ff,&ea,&aa,&00,&00,&00,&00,&88,&00,&00,&00 
defb &00,&aa,&00,&00,&00,&44,&d9,&00,&51,&ff,&82,&e6,&51,&00,&51,&44 
defb &88,&00,&dd,&f3,&00,&e6,&a2,&f3,&cc,&aa,&44,&51,&51,&00,&88,&00 
defb &00,&55,&e6,&eb,&55,&cc,&00,&ea,&5f,&aa,&cc,&c4,&d9,&88,&51,&fb 
defb &88,&44,&e6,&d9,&aa,&00,&00,&f7,&eb,&fb,&88,&00,&88,&fb,&88,&a2 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&0c 
defb &33,&26,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&39,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &11,&3c,&3c,&aa,&88,&00,&bc,&3c,&fe,&bb,&33,&76,&36,&3c,&3c,&39 
defb &fc,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&a2,&00 
defb &00,&a2,&cc,&51,&00,&00,&55,&ff,&ff,&ff,&fe,&be,&3c,&36,&3c,&3c 
defb &39,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&fe,&00,&00,&44,&00,&00,&00 
defb &fd,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&88,&44,&88,&ff,&eb,&87,&0f,&5f,&ff,&ff,&7d,&fc,&fe,&fe,&bc 
defb &fc,&f4,&f8,&fc,&fc,&36,&fd,&af,&ff,&ff,&ff,&ff,&ff,&7d,&ff,&fa 
defb &f0,&f0,&f8,&f0,&f0,&f4,&f0,&f0,&f4,&5a,&f0,&f0,&f8,&a8,&fa,&f0 
defb &f0,&fd,&f8,&f4,&fc,&f8,&f0,&f0,&f0,&f0,&f0,&af,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&cc,&f3,&f3,&f3,&f3,&e6,&f3,&f3,&f3,&cc,&ff,&fe,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&f4,&fc,&fc,&fc,&f8,&fc,&f0,&fc,&f4,&fc 
defb &f4,&f8,&f0,&f8,&f0,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f3,&fb,&a2,&dd,&51,&00,&e6,&e6,&e6,&d9,&88,&cc,&ff,&f3,&cc 
defb &a2,&dd,&ff,&fd,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&f8,&f8,&f0,&fc,&f8,&fc,&f8,&f0,&fc,&fc,&fc 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f8,&f8,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &fb,&fb,&f3,&f3,&e6,&aa,&d9,&dd,&ff,&ff,&fb,&e6,&00,&51,&00,&88 
defb &cc,&d5,&eb,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fd,&fe,&fc,&fe 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&ac,&ac,&fc,&fc,&f4,&fc,&f0,&f0,&f0,&f0,&f0,&fc,&f0,&f4,&f8 
defb &f0,&f0,&f4,&f8,&f0,&f0,&f4,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f4,&f0 
defb &44,&00,&00,&00,&00,&88,&00,&00,&00,&00,&51,&88,&51,&00,&00,&cc 
defb &d9,&cc,&00,&88,&ea,&87,&1b,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fd,&33,&bb,&77,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&ff,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&f8,&f4,&f4,&f8,&f0,&f8,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &88,&00,&44,&51,&44,&00,&00,&00,&00,&00,&44,&f3,&00,&f7,&f3,&e6 
defb &f3,&d9,&f3,&f3,&ff,&f7,&f7,&ff,&3c,&3c,&0f,&3c,&0f,&c3,&75,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&bb 
defb &ff,&fc,&fc,&fc,&54,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f4,&f4,&fc,&f8,&fc 
defb &44,&cc,&d9,&cc,&00,&44,&e6,&51,&d9,&cc,&51,&88,&00,&51,&d9,&d9 
defb &51,&cc,&d9,&a2,&cc,&f3,&e6,&51,&00,&51,&51,&c8,&d5,&2d,&0f,&1e 
defb &0f,&0f,&0f,&4b,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&ec,&cc,&fc,&fc,&fc 
defb &f3,&88,&cc,&00,&00,&44,&88,&00,&51,&00,&00,&00,&00,&00,&00,&00 
defb &00,&88,&cc,&a2,&a2,&44,&cc,&44,&00,&00,&00,&51,&51,&c3,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&75,&af,&0f,&d7,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&fd,&ff,&fe,&ff,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc 
defb &ff,&ff,&00,&00,&00,&00,&88,&00,&00,&00,&00,&00,&00,&00,&44,&d9 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&88,&a2,&d9,&ff,&eb 
defb &25,&87,&87,&4b,&0f,&0f,&25,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fd 
defb &55,&ff,&00,&00,&00,&00,&00,&88,&44,&00,&a2,&00,&00,&00,&a2,&00 
defb &00,&00,&00,&00,&00,&00,&00,&44,&f3,&88,&00,&00,&00,&41,&30,&75 
defb &df,&f3,&71,&61,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&92,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&cf,&df,&ff 
defb &00,&55,&d5,&ee,&00,&a7,&1e,&aa,&00,&00,&88,&00,&d9,&00,&00,&00 
defb &00,&51,&00,&00,&88,&00,&00,&00,&00,&51,&00,&ff,&51,&eb,&87,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&d7,&5f,&af,&0f,&5f,&0f,&0f,&0f,&0f 
defb &c3,&0f,&87,&0f,&0f,&0f,&0f,&0f,&0f,&af,&af,&0f,&61,&c3,&eb,&c3 
defb &c3,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ef,&ef,&ff 
defb &00,&00,&00,&ff,&ff,&ff,&ff,&aa,&00,&00,&00,&44,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&44,&aa,&00,&00,&88,&44,&fb,&88,&88,&c3,&c3 
defb &5f,&af,&c3,&55,&75,&87,&0f,&0f,&0f,&0f,&5f,&51,&f7,&ff,&4b,&a7 
defb &a7,&eb,&0f,&0f,&d7,&c3,&f7,&c3,&af,&af,&ea,&f7,&eb,&fb,&d9,&5f 
defb &ff,&ba,&ff,&eb,&ff,&af,&5f,&eb,&ff,&ff,&fb,&00,&f7,&ff,&ff,&ff 
defb &00,&e7,&a2,&00,&f7,&ff,&be,&44,&00,&00,&00,&45,&aa,&00,&00,&00 
defb &00,&00,&00,&00,&00,&f3,&e6,&55,&88,&e3,&a2,&ee,&00,&00,&f3,&00 
defb &00,&00,&00,&f3,&88,&88,&00,&88,&e6,&a2,&f7,&cc,&cc,&00,&44,&f3 
defb &af,&e2,&44,&f3,&55,&88,&44,&dd,&df,&a2,&88,&a2,&44,&00,&cc,&f3 
defb &ee,&44,&a2,&51,&00,&88,&44,&a2,&51,&51,&44,&00,&51,&ff,&88,&51 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&26,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &55,&3c,&a8,&00,&00,&54,&54,&be,&fa,&44,&fe,&3c,&3c,&28,&54,&3c 
defb &fe,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&aa,&00,&f5,&aa,&00,&54,&00,&00,&fe,&00 
defb &00,&00,&cc,&00,&00,&00,&00,&88,&44,&cc,&ff,&ee,&fc,&ff,&33,&ff 
defb &fe,&fd,&ff,&77,&77,&ff,&ff,&ff,&fc,&54,&00,&00,&00,&00,&00,&00 
defb &00,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&44,&51,&af,&5f,&ff,&ff,&ff,&1e,&3c,&39,&39,&33,&3c 
defb &be,&fe,&bb,&ff,&fd,&fe,&fd,&f5,&f8,&f8,&55,&f5,&ff,&f8,&bf,&f8 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&a0,&00,&f0 
defb &f0,&f0,&f0,&f4,&be,&39,&f0,&f0,&f0,&f0,&f0,&f5,&f0,&f0,&f5,&f8 
defb &f0,&f0,&f0,&f0,&f0,&f0,&b1,&be,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&00,&d9,&f3,&f3,&f3,&a2,&d9,&f3,&e6,&cc,&f3,&ff,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&fc,&fc 
defb &fc,&fc,&f8,&f0,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f0,&f0 
defb &a2,&51,&f3,&a2,&00,&00,&00,&44,&a2,&00,&d9,&44,&f3,&f3,&d9,&a2 
defb &e6,&55,&ff,&ff,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&f8,&f8,&f4,&f8,&f0,&f0,&fc,&fc,&f8,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&fc,&fc,&fc,&fc,&fc,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f0,&fc,&f0,&f0,&f0,&f0 
defb &f3,&ff,&f3,&cc,&dd,&f7,&d9,&f7,&f3,&f3,&f3,&f3,&a2,&44,&f3,&88 
defb &cc,&f3,&af,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&ff,&fe 
defb &fc,&fc,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&f0,&f0,&f0,&f8,&f4,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f8,&f0,&f0,&f4,&f4,&fc,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&88,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&44,&00,&00,&00 
defb &51,&a2,&00,&cc,&f3,&f7,&0f,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&54,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f4,&fc 
defb &f8,&f0,&f8,&f0,&f0,&f4,&f0,&f0,&f4,&f0,&f0,&fc,&fc,&f8,&f0,&f0 
defb &00,&00,&00,&00,&88,&00,&00,&00,&00,&00,&51,&e6,&f3,&d9,&d9,&f3 
defb &f3,&d9,&e6,&f3,&f7,&f7,&f3,&f3,&be,&be,&3c,&1e,&0f,&92,&75,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&fc,&dc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f8,&f0,&fc,&f8 
defb &00,&44,&cc,&cc,&88,&d9,&e6,&51,&f3,&d9,&d9,&88,&45,&51,&00,&cc 
defb &44,&e6,&d9,&00,&44,&44,&d9,&44,&e6,&51,&d9,&f3,&f7,&af,&0f,&1e 
defb &0f,&0f,&0f,&0f,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &fc,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &f7,&ff,&88,&00,&00,&cc,&88,&00,&00,&00,&00,&88,&00,&00,&00,&88 
defb &00,&88,&00,&00,&88,&fb,&88,&cc,&00,&00,&00,&51,&44,&ff,&87,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&1b,&0f,&0f,&0f,&0f,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&fe,&ff,&fe,&fc,&fd,&fc,&fc,&fd,&ff,&ff,&fe,&fc,&fc 
defb &ff,&ff,&00,&00,&00,&00,&44,&00,&00,&00,&44,&00,&00,&00,&fb,&e6 
defb &a2,&00,&00,&00,&00,&00,&00,&00,&88,&00,&00,&00,&44,&51,&f3,&75 
defb &61,&0f,&0f,&0f,&4b,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&51,&ff 
defb &55,&ff,&aa,&00,&00,&51,&00,&00,&51,&44,&00,&88,&00,&00,&00,&cc 
defb &00,&88,&00,&00,&00,&a2,&00,&00,&a2,&88,&00,&00,&51,&af,&0f,&0f 
defb &ef,&ba,&30,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&d7,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&e7,&f7,&ff 
defb &00,&45,&ff,&ff,&00,&55,&1e,&80,&00,&cc,&00,&00,&88,&00,&00,&00 
defb &00,&00,&00,&00,&88,&00,&55,&00,&00,&00,&00,&00,&f3,&ff,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&25,&0f,&0f,&0f,&4b,&ba,&0f,&0f,&0f 
defb &0f,&0f,&4b,&4b,&0f,&0f,&0f,&0f,&0f,&af,&23,&0b,&0f,&4b,&c3,&c3 
defb &4b,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ef,&ff,&ff 
defb &00,&00,&00,&ff,&ff,&ff,&ff,&00,&00,&00,&00,&00,&00,&00,&aa,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&88,&a2,&51,&f3,&00,&51,&eb,&ff 
defb &87,&a7,&00,&51,&f3,&a7,&0f,&0f,&0f,&af,&0a,&40,&d5,&5b,&f3,&ff 
defb &d7,&e3,&0f,&4b,&0a,&f3,&fb,&fb,&eb,&af,&af,&fb,&ff,&51,&55,&ff 
defb &ff,&5f,&ff,&51,&eb,&0f,&5f,&d7,&d7,&75,&ff,&00,&dd,&ff,&ff,&ff 
defb &45,&e7,&88,&00,&55,&ff,&ff,&fb,&aa,&00,&00,&55,&d3,&8a,&00,&00 
defb &a2,&00,&44,&00,&00,&f3,&e6,&51,&44,&c8,&55,&00,&00,&00,&ee,&00 
defb &88,&00,&00,&a2,&88,&44,&a2,&88,&00,&51,&f3,&44,&00,&00,&00,&e6 
defb &4b,&e3,&00,&f3,&ee,&e6,&d9,&e6,&00,&00,&cc,&e6,&00,&51,&cc,&44 
defb &ee,&d9,&00,&00,&00,&a2,&44,&00,&44,&88,&a2,&00,&cc,&51,&a2,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&39,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&39,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&77,&a8,&00,&00,&00,&00,&54,&00,&00,&44,&ee,&54,&88,&55,&36 
defb &fe,&14,&3c,&7d,&bb,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&aa,&00,&fe,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&55,&aa,&00,&05,&a8,&55,&aa,&00,&00,&00,&00 
defb &00,&00,&aa,&88,&00,&00,&00,&45,&00,&55,&dd,&aa,&00,&fe,&ff,&f8 
defb &ff,&ff,&fd,&00,&f4,&00,&00,&00,&00,&54,&00,&00,&00,&00,&00,&00 
defb &00,&bc,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&00,&44,&d9,&ff,&77,&ff,&ff,&ff,&ff,&ff,&fe,&ff,&ff,&ff 
defb &fc,&f5,&ff,&ff,&ff,&ff,&fe,&ff,&ff,&ff,&fd,&ff,&fd,&fe,&fb,&fa 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&a0,&50,&f0 
defb &f0,&f0,&f0,&f0,&f0,&5f,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f4,&f8 
defb &f4,&f0,&f0,&f0,&f4,&f0,&fd,&fe,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&00,&d9,&f3,&e6,&d9,&e6,&d9,&d9,&e6,&00,&44,&dd,&fe,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f0,&f8,&f0 
defb &f8,&fc,&fc,&f4,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f0,&f0,&f4 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f3,&a2,&cc,&00,&00,&00,&00,&00,&44,&cc,&44,&d9,&44,&f3,&a2 
defb &00,&55,&ff,&7d,&ff,&fe,&fc,&fd,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&f0,&fc,&f4,&fc,&f8,&f0,&f0,&f0,&f0,&fc 
defb &f0,&f8,&f8,&f8,&f0,&f0,&f0,&f0,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc 
defb &f0,&f0,&f4,&f4,&f0,&f0,&f0,&f0,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f7,&f3,&44,&cc,&f3,&f3,&f3,&f3,&d9,&f3,&f3,&f7,&f3,&cc,&f3 
defb &d9,&88,&c3,&1a,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc 
defb &ff,&fe,&ff,&ff,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f0,&f0,&f0,&f4,&f0,&f4,&f8,&f0 
defb &f0,&f4,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f8,&fc,&fc 
defb &88,&a2,&00,&aa,&00,&00,&00,&00,&00,&00,&00,&00,&44,&51,&00,&00 
defb &51,&00,&a2,&88,&f3,&f3,&af,&1e,&c2,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&33,&ff,&ff,&fe,&fc,&fc,&fd,&fc,&fc,&fc 
defb &ff,&76,&b9,&76,&ff,&fc,&fc,&fd,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8 
defb &f4,&f8,&f0,&f4,&f0,&fc,&fc,&f8,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &00,&00,&00,&44,&00,&55,&e6,&00,&cc,&e6,&e6,&44,&d9,&d9,&f3,&f3 
defb &f3,&f3,&f3,&f3,&fb,&f3,&f3,&f7,&ea,&ff,&be,&3c,&3c,&0f,&92,&30 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &77,&ff,&ff,&ff,&ff,&fe,&ff,&fc,&fc,&fc,&fc,&fc,&fe,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f8,&f0 
defb &00,&cc,&e6,&cc,&00,&00,&51,&a2,&51,&f3,&f3,&00,&45,&45,&51,&d9 
defb &00,&cc,&d9,&d9,&51,&f3,&aa,&e6,&a2,&a2,&44,&88,&f3,&ea,&0f,&3c 
defb &0f,&0f,&0f,&0f,&92,&ff,&ff,&ff,&5f,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &f7,&3c,&cc,&00,&44,&44,&00,&cc,&00,&00,&88,&d9,&00,&00,&44,&00 
defb &00,&00,&00,&00,&88,&00,&88,&00,&88,&00,&00,&44,&44,&e6,&af,&0f 
defb &4b,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&5f,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&fd,&ff 
defb &ff,&ff,&00,&00,&00,&00,&44,&88,&00,&00,&44,&00,&00,&51,&d9,&88 
defb &00,&00,&00,&00,&88,&00,&00,&00,&88,&00,&00,&00,&00,&51,&e6,&ba 
defb &30,&c3,&0f,&c3,&c3,&b2,&0f,&af,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&4b,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&f3,&ff,&fb,&ff,&ff,&55,&ff 
defb &55,&ff,&aa,&00,&00,&00,&00,&00,&51,&00,&00,&00,&00,&44,&00,&55 
defb &cc,&00,&00,&00,&00,&00,&00,&f7,&55,&cc,&00,&e6,&e2,&0f,&0f,&0f 
defb &c3,&0f,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&c3,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fb,&88,&ff,&ff 
defb &00,&00,&ff,&ff,&00,&55,&ff,&aa,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&44,&00,&00,&00,&00,&00,&00,&dd,&f7,&ff,&5f,&eb 
defb &af,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&eb,&c3,&e3,&55,&0f,&0f,&0f 
defb &25,&87,&0f,&4b,&0f,&0f,&0f,&0f,&0f,&af,&0f,&0b,&0f,&0f,&c3,&c3 
defb &0f,&4b,&d7,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ef,&eb,&ff 
defb &00,&00,&00,&df,&ff,&ff,&ff,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&aa,&00,&00,&00,&51,&51,&f3,&44,&51,&ff,&51 
defb &55,&a7,&a2,&00,&41,&dd,&0f,&0f,&1a,&d7,&aa,&e2,&51,&0a,&51,&aa 
defb &d7,&87,&4b,&5f,&5f,&00,&fb,&00,&30,&ff,&f7,&fb,&ee,&00,&ea,&5f 
defb &ff,&55,&f7,&aa,&eb,&0f,&5f,&c1,&d5,&75,&f3,&88,&ff,&ff,&ff,&ff 
defb &51,&8a,&00,&00,&55,&ff,&77,&fb,&00,&00,&00,&a7,&71,&aa,&00,&00 
defb &00,&00,&00,&00,&00,&e6,&44,&fb,&44,&44,&f7,&00,&00,&44,&44,&a2 
defb &88,&00,&44,&44,&00,&cc,&a2,&00,&cc,&44,&f7,&88,&00,&00,&cc,&cc 
defb &a2,&ee,&00,&a2,&00,&88,&d9,&88,&51,&00,&00,&f3,&aa,&88,&44,&d9 
defb &fb,&44,&51,&a2,&cc,&00,&00,&44,&88,&88,&a2,&00,&44,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&33,&33,&33,&33,&33,&33,&33,&33 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &36,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&33,&3c,&3c,&39,&33,&33,&33,&33,&33 
defb &3c,&3c,&3c,&3c,&3c,&39,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&aa,&00,&00,&00,&00,&00,&00,&44,&00,&00,&00,&00,&00,&00,&fd 
defb &00,&ee,&a8,&fc,&ff,&be,&3c,&be,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &51,&ff,&eb,&ee,&00,&88,&d9,&aa,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&aa,&00,&00,&00,&00,&cc,&51,&f7,&88,&55,&44,&00,&00,&45,&00 
defb &00,&00,&aa,&00,&00,&00,&00,&00,&00,&00,&00,&aa,&00,&aa,&ff,&00 
defb &00,&dd,&55,&55,&44,&00,&00,&00,&00,&a0,&00,&00,&00,&00,&00,&00 
defb &00,&55,&bb,&33,&77,&33,&33,&33,&33,&77,&fc,&fc,&fc,&fc,&fc,&fc 
defb &88,&88,&00,&a2,&88,&55,&ff,&ff,&ff,&aa,&fe,&fc,&fc,&f8,&fc,&a0 
defb &fc,&f5,&f8,&fa,&f0,&55,&fe,&88,&fe,&fe,&f5,&fd,&55,&f5,&a0,&fa 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f4,&f0,&f4,&fc,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&cc,&51,&f3,&f3,&51,&f3,&f3,&f3,&f3,&00,&55,&dd,&ff,&fe,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&fc,&f8,&f0,&fc 
defb &fc,&f8,&f4,&f8,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f3,&a2,&51,&f3,&44,&00,&00,&44,&cc,&cc,&88,&44,&cc,&d9,&d9 
defb &00,&f7,&ff,&ff,&ff,&ff,&fc,&ff,&74,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f8,&f0,&f4,&fc,&fc,&f8,&f0,&fc 
defb &f8,&fc,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&fc,&f4,&fc,&fc,&fc 
defb &fc,&fc,&f0,&f8,&f0,&fc,&f8,&f0,&f0,&f0,&f0,&f4,&f0,&f0,&f0,&f0 
defb &f3,&fb,&f3,&d9,&ee,&f3,&e6,&d9,&f3,&f3,&f3,&f3,&f3,&f3,&f3,&e6 
defb &cc,&cc,&af,&5f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&33,&ff 
defb &ff,&ff,&fe,&fd,&fc,&ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&f4,&f0,&fc,&fc,&fc,&fc,&f0,&f4,&fc,&f8,&f0,&f0 
defb &f0,&f0,&f0,&f0,&fc,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&fc 
defb &d9,&51,&e6,&88,&00,&00,&00,&00,&00,&00,&00,&00,&00,&e6,&51,&00 
defb &00,&51,&00,&44,&f3,&f3,&af,&2d,&0f,&c2,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fd,&ff,&ff,&ff,&fd,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&df,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&f8,&f4,&f8,&fc,&fc,&fc,&fc,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &00,&00,&00,&00,&a2,&cc,&dd,&cc,&cc,&88,&00,&44,&88,&00,&51,&a2 
defb &a2,&e6,&f3,&51,&e6,&d9,&ee,&f3,&ff,&ff,&af,&3c,&3c,&0f,&0f,&c3 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&f7,&ff,&ff,&fc,&fd,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &00,&44,&a2,&88,&cc,&00,&51,&51,&f3,&d9,&00,&d9,&f3,&f3,&a2,&f3 
defb &00,&44,&f3,&f3,&f3,&51,&ee,&88,&a2,&00,&d9,&88,&88,&55,&0f,&1e 
defb &0f,&0f,&0f,&0f,&1a,&30,&75,&87,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &fc,&fc,&fc,&ec,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &f7,&ff,&88,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&88,&00,&00,&00,&cc,&00,&00,&cc,&cc,&a2,&af,&0f 
defb &4b,&c3,&0f,&0f,&0f,&0f,&0f,&0f,&1e,&1e,&0f,&0f,&0f,&0f,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&fc,&ec,&cc,&cc,&fc,&fc,&fc 
defb &ff,&ff,&00,&00,&00,&00,&aa,&a2,&00,&00,&00,&00,&00,&00,&44,&00 
defb &a2,&00,&00,&00,&00,&00,&00,&00,&88,&51,&00,&51,&00,&cc,&f3,&30 
defb &30,&ba,&0f,&0f,&87,&aa,&ff,&0f,&3c,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&4b,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&f7,&ff,&dd,&fb,&df,&ff 
defb &00,&ff,&ee,&00,&00,&00,&00,&00,&00,&00,&00,&51,&f3,&00,&00,&00 
defb &88,&00,&00,&00,&00,&44,&00,&00,&44,&dd,&51,&88,&e3,&af,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&c3,&c3,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&c3,&75,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fb,&f2,&fd,&ff 
defb &00,&00,&ff,&ff,&00,&55,&2d,&80,&00,&00,&00,&00,&88,&00,&00,&00 
defb &00,&00,&00,&00,&00,&54,&00,&44,&aa,&00,&44,&aa,&df,&af,&c3,&d7 
defb &aa,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&ff,&40,&f7,&eb,&0f,&0f 
defb &eb,&0f,&0f,&87,&0f,&0f,&0f,&0f,&d5,&af,&0f,&0f,&4b,&87,&c3,&c3 
defb &0f,&0f,&c3,&d5,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ba,&ff 
defb &00,&00,&00,&55,&ff,&ff,&ff,&aa,&00,&00,&00,&00,&00,&8a,&45,&8a 
defb &00,&00,&44,&00,&00,&a2,&e6,&88,&44,&55,&fb,&d9,&44,&88,&eb,&e6 
defb &00,&05,&aa,&d9,&05,&ff,&0f,&0f,&5f,&55,&fb,&d7,&51,&e6,&cc,&a2 
defb &ff,&87,&1a,&5f,&cb,&55,&ff,&c3,&eb,&ff,&cc,&dd,&f3,&51,&eb,&c2 
defb &ff,&dd,&51,&aa,&fb,&af,&ff,&ff,&92,&fb,&88,&f3,&ff,&ff,&ff,&ff 
defb &00,&00,&44,&00,&45,&ff,&ff,&f3,&88,&00,&51,&e3,&fb,&ef,&00,&00 
defb &00,&00,&00,&00,&00,&cc,&00,&f3,&51,&51,&a2,&88,&44,&44,&44,&00 
defb &88,&00,&00,&00,&00,&88,&cc,&51,&a2,&00,&f3,&00,&00,&44,&cc,&f3 
defb &cc,&cc,&88,&00,&00,&cc,&cc,&51,&88,&a2,&51,&88,&a2,&44,&44,&d9 
defb &44,&cc,&cc,&e6,&88,&44,&00,&cc,&44,&00,&82,&00,&88,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&39 
defb &33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33,&33 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c,&0c 
defb &1c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&39,&33,&33 
defb &36,&3c,&3c,&3c,&3c,&33,&36,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&54,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&cc 
defb &00,&00,&88,&44,&00,&54,&fc,&55,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c,&3c 
defb &00,&00,&51,&51,&cc,&f3,&d9,&88,&00,&00,&88,&00,&00,&00,&00,&00 
defb &44,&ee,&d9,&00,&00,&00,&00,&44,&55,&e6,&51,&f7,&f7,&ff,&ff,&ff 
defb &fb,&ff,&ff,&f3,&ff,&ff,&ee,&d9,&44,&88,&dd,&44,&44,&00,&ff,&00 
defb &a2,&55,&44,&00,&dd,&a2,&f3,&dd,&e6,&dd,&aa,&00,&00,&00,&00,&f5 
defb &00,&f5,&f4,&fc,&fc,&fc,&fc,&f4,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &cc,&d9,&51,&44,&e6,&dd,&fb,&f7,&fe,&fe,&fe,&fc,&fc,&a8,&fc,&a8 
defb &fc,&00,&fa,&fe,&f8,&00,&f0,&00,&00,&00,&50,&f4,&50,&f0,&00,&fa 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f4,&f0,&00,&00,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f3,&f3,&f3,&f3,&f3,&f3,&f7,&fb,&e6,&f3,&cc,&44,&d9,&f3,&fe,&fe 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&f4,&f4,&f0,&f0,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f8,&f8,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &e6,&d9,&a2,&ee,&f3,&d9,&00,&00,&44,&88,&44,&00,&44,&51,&cc,&d9 
defb &00,&f3,&ff,&55,&ff,&ff,&ff,&fb,&5e,&fc,&fc,&fc,&fd,&ff,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f4,&ac,&fc,&f8,&f0,&fc,&f0,&f0 
defb &fc,&fc,&f4,&fc,&fc,&f0,&f0,&f0,&f0,&f4,&f0,&f4,&fc,&f4,&fc,&fc 
defb &fc,&fc,&f4,&fc,&fc,&f4,&fc,&fc,&fc,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &d9,&d9,&44,&d9,&88,&e6,&d9,&f3,&f3,&f3,&e6,&d9,&e6,&d9,&f3,&e6 
defb &cc,&d9,&ea,&af,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&fd,&ff,&fc,&fc,&fd,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&f4,&fc,&fc,&f8,&f0,&f0,&f0,&f4 
defb &f0,&f0,&f0,&f0,&f0,&f0,&f8,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0,&f0 
defb &44,&44,&88,&cc,&00,&00,&00,&00,&00,&00,&00,&00,&00,&88,&88,&00 
defb &00,&44,&00,&00,&f3,&f3,&ff,&be,&27,&0f,&0f,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&bb,&ff,&fe,&fe,&fd 
defb &fc,&fc,&fc,&b9,&55,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&f8,&fc,&fc,&fc,&fc,&fc,&f0,&f4,&f0,&f0,&f0,&f0 
defb &00,&00,&00,&51,&d9,&d9,&f3,&cc,&cc,&44,&00,&00,&00,&00,&51,&e6 
defb &a2,&88,&88,&44,&44,&44,&cc,&d9,&f3,&f7,&ff,&2d,&3c,&3c,&0f,&c3 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&77,&ff,&ff,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &aa,&51,&a2,&cc,&55,&f3,&00,&51,&88,&cc,&88,&00,&cc,&f3,&44,&cc 
defb &44,&cc,&88,&44,&f3,&51,&cc,&d9,&e6,&44,&d9,&55,&cc,&af,&0f,&3c 
defb &0f,&0f,&0f,&0f,&0f,&30,&75,&ff,&0f,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&f7,&ff,&ff,&ff,&ff,&ff,&fe,&fc 
defb &ff,&fe,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc,&fc 
defb &aa,&bb,&88,&00,&00,&44,&88,&00,&00,&00,&44,&00,&88,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&44,&88,&00,&00,&cc,&00,&eb,&0f 
defb &0f,&87,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&2d,&0f,&0f,&0f,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&ec,&fc,&ec,&cc,&cc 
defb &ff,&ff,&00,&00,&00,&00,&00,&88,&00,&00,&00,&00,&88,&a2,&00,&44 
defb &e6,&00,&00,&00,&00,&00,&00,&55,&cc,&cc,&00,&44,&88,&44,&f3,&92 
defb &30,&10,&61,&0f,&0f,&5f,&bb,&33,&3c,&0f,&0f,&0f,&0f,&0f,&0f,&0f 
defb &0f,&0f,&4b,&0f,&0f,&0f,&0f,&5f,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&f3,&aa,&f7,&fe 
defb &00,&ff,&ff,&44,&44,&44,&00,&00,&00,&00,&88,&51,&d9,&00,&00,&00 
defb &ee,&00,&44,&44,&00,&00,&00,&00,&cc,&51,&00,&88,&f3,&0f,&0f,&0f 
defb &0f,&0f,&0f,&ba,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&c3,&87,&0f,&0f 
defb &0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&4b,&c3,&ff,&ff,&ff 
defb &ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&ff,&fe,&a9,&ff 
defb &00,&00,&9b,&ff,&aa,&55,&bb,&ee,&44,&00,&00,&00,&00,&00,&00,&00 
defb &88,&00,&00,&00,&00,&44,&ee,&51,&88,&00,&00,&00,&f7,&e3,&87,&ff 
defb &aa,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&0f,&5f,&51,&f7,&ff,&0f,&0f 
defb &ff,&5f,&0f,&0f,&87,&0f,&0f,&0f,&4b,&ff,&af,&0f,&4b,&85,&4b,&c3 
defb &87,&0f,&87,&4b,&d7,&ff,&ff,&ff,&ff,&f3,&ff,&ff,&ff,&ff,&ba,&ff 
defb &00,&00,&00,&55,&ff,&ff,&ff,&00,&00,&00,&00,&00,&aa,&00,&00,&aa 
defb &00,&00,&44,&00,&00,&00,&88,&00,&44,&dd,&ff,&00,&44,&00,&aa,&f3 
defb &80,&dd,&a2,&41,&d3,&eb,&0f,&4b,&f7,&00,&00,&fb,&51,&a2,&88,&cc 
defb &a2,&4b,&ff,&eb,&55,&fb,&55,&fb,&65,&fb,&ee,&d9,&f3,&51,&ff,&fb 
defb &dd,&55,&44,&aa,&88,&f7,&d3,&f7,&5f,&aa,&a2,&d9,&ff,&ff,&fb,&ff 
defb &00,&00,&00,&88,&00,&ff,&ff,&fb,&00,&00,&51,&b2,&fb,&aa,&00,&00 
defb &00,&00,&00,&00,&00,&a2,&88,&51,&cc,&44,&a2,&00,&51,&88,&44,&00 
defb &88,&44,&88,&00,&00,&88,&f3,&a2,&f3,&55,&a2,&51,&00,&00,&d9,&a2 
defb &a2,&a2,&e6,&00,&00,&cc,&cc,&00,&51,&00,&51,&00,&a2,&44,&44,&cc 
defb &00,&44,&d9,&cc,&00,&44,&00,&00,&44,&00,&aa,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00 
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00,&00
;; scr

;; pixel data for the sprite
sprite_pixels:
defb &00,&00,&50,&a0,&00,&00,&00,&50,&f0,&f0,&a0,&00,&00,&50,&f0,&f0 
defb &a0,&00,&00,&f0,&f0,&f0,&f4,&00,&50,&f0,&0f,&f0,&f4,&a8,&50,&b4 
defb &0e,&f0,&f0,&a8,&50,&a5,&5a,&f0,&f0,&a8,&50,&a5,&78,&f0,&f0,&a8 
defb &50,&a4,&b0,&70,&f0,&a0,&f0,&a4,&b0,&b0,&f0,&e4,&f0,&f0,&b0,&b0 
defb &f0,&e4,&f0,&f0,&a1,&52,&f0,&e4,&f0,&f0,&a1,&a1,&f0,&e4,&50,&f0 
defb &e1,&e1,&58,&a0,&50,&f0,&e1,&d2,&78,&a0,&50,&f0,&f0,&f0,&78,&a0 
defb &50,&f0,&f0,&1c,&f0,&a8,&00,&f0,&f0,&f0,&e4,&a8,&00,&f0,&f0,&f0 
defb &f4,&00,&00,&50,&f0,&f0,&a8,&00,&00,&50,&cc,&fc,&a8,&00,&00,&00 
defb &44,&a8,&00,&00
;; spr

end start
