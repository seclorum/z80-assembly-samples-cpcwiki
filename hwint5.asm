;; This example shows another way to handle the interrupts by hitting the hardware
;; directly.
;;

org &4000

start:
;; disable interrupts
di				
;; set interrupt mode 1
im 1			

;; poke EI:RET to &0038 to make a dummy interrupt handler
ld hl,&c9fb	;; C9 = RET, FB = EI
ld (&0038),hl

;; enable interrupts
ei

mainloop:
ld b,&f5
ml1:
in a,(c)
rra
jr nc,ml1

;; this will be 2 HSYNC after VSYNC if we don't do much work here
halt

;; do some work here (less than 52 lines)
halt

;; do some work here (less than 52 lines)
halt

;; do some work here (less than 52 lines)
halt

;; do some work here (less than 52 lines)
halt

;; do some work here (less than 52 lines)
halt

jp mainloop

end start