;; firmware functions we use in this example
.kl_new_fast_ticker equ &bce0
.mc_set_mode        equ &bd1c
.mc_wait_flyback    equ &bd19
.kl_init_event equ &bcef

;; this code must be in the range &4000-&bfff
;; to work correctly.
;;
;; assemble, then from BASIC:
;; 
;; call &8000
org &8000

;; wait for a screen refresh
;; we do this to synchronise our effect
;; and so that the first ticker interrupt is in a predictable place
call mc_wait_flyback

ld a,6
ld (ticker_counter),a
ld hl,modes
ld (current_mode_pointer),hl

;; install interrupt
ld hl,ticker_event_block
ld b,%10000010				;; asynchronous event, priority 1
ld c,&80					;; rom select 
ld de,ticker_function
call kl_new_fast_ticker

;; return to BASIC
ret

;; this is initialised by
;; the firmware; holds runtime state of ticker interrupt
.ticker_event_block
defs 10

;; this is the function called each 1/300th of a second
.ticker_function
push af
push hl

;; The 1/300th of a second interrupt effectively splits
;; the screen into 6 sections of equal height. Each section
;; spans the entire width of the screen.
;;
;; We want to ensure that the effect is stationary so we reset
;; every 6 calls of this function.
ld a,(ticker_counter)
dec a
ld (ticker_counter),a
or a
jr nz,ticker_function2
ld a,6
ld (ticker_counter),a
ld hl,modes
ld (current_mode_pointer),hl

.ticker_function2

;; get pointer to current mode
ld hl,(current_mode_pointer)
;; get the mode
ld a,(hl)
;; update pointer
inc hl
;; store pointer
ld (current_mode_pointer),hl

;; set mode. This will take effect at the start of the next horizontal
;; scan-line. This firmware function talks direct to the hardware.
;; The screen is not cleared and the other firmware functions for
;; plotting to the screen have no knowledge of the new mode.
call mc_set_mode

pop af
pop hl
ret

.ticker_counter defb 0

.current_mode_pointer defw modes

;; The 1/300th of a second interrupt effectively splits
;; the screen into 6 sections of equal height. Each section
;; spans the entire width of the screen.
;;
;; video mode for each of the 6 sections of the screen.

.modes
defb 0
defb 1
defb 2
defb 0
defb 1
defb 2


