;; This example shows how to avoid the problem area when scrolling vertically.
;;
;; We setup the width of the screen so that &400/width has no remainder.
;;
;; In this example the problem area is highlighted and remains on the right side of the screen.
;;
;; This means we can perform hardware scrolling and use faster sprite drawing functions that
;; do not need to consider the problem area. They still need to use the special scr_next_line.
org &8000


scr_width equ 32

km_read_key equ &bb1b
mc_wait_flyback equ &bd19
scr_set_mode equ &bc0e
txt_output	equ &bb5a

start:

;; set the screen mode
ld a,1
call scr_set_mode

;; fill screen with chars
ld bc,24*40
ld d,' '
l1:
inc d
ld a,d
cp &7f
jr nz,no_char_reset
ld d,' '
no_char_reset:
ld a,d
call txt_output
dec bc
ld a,b
or c
jr nz,l1

;; highlight problem area
ld a,&ff
ld (&c7ff),a
ld (&cfff),a
ld (&d7ff),a
ld (&dfff),a
ld (&e7ff),a
ld (&efff),a
ld (&f7ff),a
ld (&ffff),a

;; set scr width
ld bc,&bc01
out (c),c
ld bc,&bd00+scr_width
out (c),c

loop:
;; wait for vsync 
;;
;; (synchronises scroll with the screen refresh; as a result it
;; updates at a constant rate and is smooth)
call mc_wait_flyback

;; write scroll parameters to hardware
call update_scroll

call set_scroll

;; loop
jp loop

;;--------------------------------------------------------------------------------------------

update_scroll:

ld hl,(scroll_offset)
ld bc,scr_width
add hl,bc
ld (scroll_offset),hl
ret

;;--------------------------------------------------------------------------------------------
;; set scroll parameters in hardware
set_scroll:
ld hl,(scroll_offset)

;; write scroll offset (in CRTC character width units)
ld bc,&bc0c				;; select CRTC register 12
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write

;; combine with scroll base
ld a,(scroll_base)
or h
out (c),a

ld bc,&bc0d				;; select CRTC register 13
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write
out (c),l
ret

;;--------------------------------------------------------------------------------------------


;; high byte of the screen base address
;; &00 -> screen uses &0000-&3fff
;; &10 -> screen uses &4000-&7fff
;; &20 -> screen uses &8000-&bfff
;; &30 -> screen uses &c000-&ffff

scroll_base: 
defb &30

;; the scroll offset in CRTC character units
scroll_offset:
defw 0

end start