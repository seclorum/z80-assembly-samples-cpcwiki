;; This example shows another way to handle the interrupts by hitting the hardware
;; directly.
;;
;; There are 6 interrupts per screen, one of which is triggered 2 HSYNCs after the start
;; of the VSYNC signal. We can use this to syncronise our interrupts.
;;
;; We first need to synchronise with the VSYNC, we can then enable our interrupts.
;; We now know exactly the order the interrupts will come and we can have 1 function for
;; each.

org &4000

start:
di				;; disable interrupts
im 1			;; Z80 interrupt mode (z80 calls to &0038 for each interrupt)

ld a,&c3	;; Z80 JP instruction
ld hl,int_fn	;; we always call this function for interrupt
ld (&0038),a	;; write JP instruction
ld (&0039),hl	;; write address
ei

;; delay
loop:
jp loop


;;--------------------------------------------------------------------------------------------

;; this is synchronised to happen 2 HSYNCs after VSYNC
int_fn:
push bc
ld b,&f5
in a,(c)
rra
jr nc,int_fn_not_vsync

;; this is the interrupt that has triggered 2 HSYNC after VSYNC
;; VSYNC must be at least 2 lines long for this to be true.

jp int_fn_end


int_fn_not_vsync:
;; int not in vsync

int_fn_end:
pop bc
ei
reti

end start
