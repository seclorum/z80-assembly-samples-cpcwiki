;; This example shows how to draw a 6 (six) pixel wide
;; font using the firmware.
;;
;; Here we use the graphics functions to define the pixel location
;; of the chars, note that the firmware uses coordinates which are effectively
;; mode 2 in the width and two times in the Y. (e.g. 640x400 screen. The reason
;; is to maintain a correct aspect ratio).
;;
;; Here we are plotting chars in mode 1.

org &4000
nolist

txt_set_graphic equ &bb63
gra_move_absolute equ &bbc0
txt_set_matrix equ &bba8
txt_set_m_table equ &bbab
gra_wr_char equ &bbfc
scr_set_mode equ &bc0e

;; drawing a six pixel wide char using the firmware

ld a,1
call scr_set_mode

;; define location of matrix table
;; to allow us to define all chars from ' ' to &ff.
ld de,' '
ld hl,matrix_table
call txt_set_m_table

;; now copy pixels into the table
;; source pixels are in mode 2 format
;; firmware will convert them to mode 1 automatically
ld hl,font_chars
ld de,matrix_table
ld bc,256*8
ldir

;; define x "char" coordinate. These new char coordinates are for 6
;; pixel wide chars.
;;
;; 1 is the "char" coordinate, 6 is the width of the char in pixels
;; 2 is the factor to convert from mode 2 to mode 1 sized pixels

ld hl,1*6*2
ld (xcoord),hl
;; define y "char" coordinate
;; 4 is the "char" coordinate, 8 is the width of the char in pixels
;; 2 is the factor to convert to y coordinates

ld hl,4*8*2
ld (ycoord),hl

ld hl,test_string
call draw_string
ret

;; draw a string of chars
;; HL = string to draw
draw_string:
push hl
;; turn on using print using graphics
ld a,&ff
call txt_set_graphic
pop hl

draw_string_loop:
ld a,(hl)
inc hl
or a
jr z,draw_string_end

push hl
push bc
push de

;; set graphics coordinates
push af
ld de,(xcoord)
ld hl,(ycoord)
call gra_move_absolute
pop af

;; and draw char to screen using firmware
call gra_wr_char

ld hl,(xcoord)
ld de,6*2		;; in mode 1
;; remember all pixels are in terms of mode 2
add hl,de
ld (xcoord),hl

pop de
pop bc
pop hl
jr draw_string_loop

draw_string_end:
;; turn off print using graphics
ld a,0
call txt_set_graphic
ret

test_string: 
defb "THIS IS A TEST",0

xcoord:
defw 0
ycoord:
defw 0

;; the UDG data for the chars
font_chars:
defb &00,&00,&00,&00,&00,&00,&00,&00,&00,&10,&10,&10,&10,&00,&10,&00,&00 
defb &28,&28,&00,&00,&00,&00,&00,&00,&28,&7c,&28,&28,&7c,&28,&00,&00 
defb &10,&7c,&50,&7c,&14,&7c,&10,&60,&64,&08,&10,&20,&4c,&0c,&00,&10 
defb &28,&28,&30,&54,&48,&34,&00,&10,&10,&20,&00,&00,&00,&00,&00,&08 
defb &10,&20,&20,&20,&10,&08,&00,&20,&10,&08,&08,&08,&10,&20,&00,&00 
defb &00,&28,&10,&7c,&10,&28,&00,&00,&00,&10,&10,&7c,&10,&10,&00,&00 
defb &00,&00,&00,&00,&20,&20,&40,&00,&00,&00,&00,&7c,&00,&00,&00,&00 
defb &00,&00,&00,&00,&30,&30,&00,&00,&00,&04,&08,&10,&20,&40,&00,&00 
defb &38,&4c,&54,&54,&64,&38,&00,&00,&30,&50,&10,&10,&10,&7c,&00,&00 
defb &38,&44,&04,&38,&40,&7c,&00,&00,&38,&44,&18,&04,&44,&38,&00,&00 
defb &08,&18,&28,&48,&7c,&08,&00,&00,&7c,&40,&78,&04,&44,&38,&00,&00 
defb &38,&40,&78,&44,&44,&38,&00,&00,&7c,&04,&08,&10,&20,&20,&00,&00 
defb &38,&44,&38,&44,&44,&38,&00,&00,&38,&44,&44,&3c,&04,&38,&00,&00 
defb &00,&00,&20,&00,&00,&20,&00,&00,&00,&10,&00,&00,&10,&10,&20,&00 
defb &00,&08,&10,&20,&10,&08,&00,&00,&00,&00,&7c,&00,&7c,&00,&00,&00 
defb &00,&20,&10,&08,&10,&20,&00,&00,&38,&44,&08,&10,&00,&10,&00,&70 
defb &00,&70,&48,&48,&48,&48,&00,&00,&38,&44,&44,&7c,&44,&44,&00,&00 
defb &78,&44,&78,&44,&44,&78,&00,&00,&38,&44,&40,&40,&44,&38,&00,&00 
defb &78,&24,&24,&24,&24,&78,&00,&00,&7c,&40,&78,&40,&40,&7c,&00,&00 
defb &7c,&40,&78,&40,&40,&40,&00,&00,&38,&44,&40,&4c,&44,&38,&00,&00 
defb &44,&44,&7c,&44,&44,&44,&00,&00,&38,&10,&10,&10,&10,&38,&00,&00 
defb &04,&04,&04,&44,&44,&38,&00,&00,&48,&50,&60,&50,&48,&44,&00,&00 
defb &40,&40,&40,&40,&40,&7c,&00,&00,&44,&6c,&54,&44,&44,&44,&00,&00 
defb &44,&64,&54,&4c,&44,&44,&00,&00,&38,&44,&44,&44,&44,&38,&00,&00 
defb &78,&44,&44,&78,&40,&40,&00,&00,&38,&44,&44,&54,&48,&34,&00,&00 
defb &78,&44,&44,&78,&48,&44,&00,&00,&38,&40,&38,&04,&44,&38,&00,&00 
defb &7c,&10,&10,&10,&10,&10,&00,&00,&44,&44,&44,&44,&44,&38,&00,&00 
defb &44,&44,&44,&44,&28,&10,&00,&00,&44,&44,&44,&44,&54,&28,&00,&00 
defb &44,&28,&10,&10,&28,&44,&00,&00,&44,&44,&28,&10,&10,&10,&00,&00 
defb &7c,&08,&10,&20,&40,&7c,&00,&00

matrix_table: defs 256*8