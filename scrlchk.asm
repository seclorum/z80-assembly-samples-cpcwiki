;; "Chunky" horizontal scroll
;;
;; This code demonstrates a method to scroll the screen, using the hardware,
;; in normal scroll units. 
;; 
;; But:
;; 1. The scroll scrolls continuous
;; 2. The scroll is scrolled as if it was scrolling at a pixel by pixel rate but
;; only scrolls at a 2 byte rate. (for example, the kind of scroll you would see on an
;; MSX 1 game).
;; 3. sprites move much smoother compared to the scroll. (sprite moves in byte coordinates
;; but we could make it smoother by moving pixel by pixel)
;;
;; make sure code is not near back buffer
org &8000


km_read_key equ &bb1b
mc_wait_flyback equ &bd19
scr_set_mode equ &bc0e
txt_output	equ &bb5a

sprite_width equ 8
sprite_height equ 16


start:

;; set the screen mode
ld a,1
call scr_set_mode

;; fill screen with chars
ld bc,24*40
ld d,' '
l1:
inc d
ld a,d
cp &7f
jr nz,no_char_reset
ld d,' '
no_char_reset:
ld a,d
call txt_output
dec bc
ld a,b
or c
jr nz,l1

;; copy to make back buffer
ld hl,&c000
ld de,&4000
ld bc,&4000
ldir
;; highlight problem area
ld a,&ff
ld (&c7ff),a
ld (&cfff),a
ld (&d7ff),a
ld (&dfff),a
ld (&e7ff),a
ld (&efff),a
ld (&f7ff),a
ld (&ffff),a

loop:
;; wait for vsync 
;;
;; (synchronises scroll with the screen refresh; as a result it
;; updates at a constant rate and is smooth)
call mc_wait_flyback

;; restore background (where sprite used to be)
ld hl,(sprite_coords)
ld b,sprite_height
ld c,sprite_width
call sprite_background_restore

;; write scroll parameters to hardware
call update_scroll

call set_scroll

;; check if any keys are pressed (they will update the sprite)
call check_keys

;; delay long enough for the vsync to become inactive 
;; so that mc_wait_flyback will synchronise with the start of the 
;; vsync.
halt


;; draw sprite in new location
ld hl,(sprite_coords)
ld b,sprite_height
ld c,sprite_width
call sprite_draw
;; loop
jp loop

;;----------------------------------------------------------------------
check_keys:
;;  test if any key has been pressed
call km_read_key
ret nc
;; A = code of the key that has been pressed
;;
;; check the codes we are using and handle appropiatly.
;; A = ascii char of key pressed
;; we check for both upper and lower case chars
cp 'q'
jp z,sprite_up
cp 'Q'
jp z,sprite_up
cp 'a'
jp z,sprite_down
cp 'A'
jp z,sprite_down
cp 'o'
jp z,sprite_left
cp 'O'
jp z,sprite_left
cp 'p'
jp z,sprite_right
cp 'P'
jp z,sprite_right
ret

sprite_left:
ld a,(xcoord)
or a
ret z
dec a
ld (xcoord),a
ret


;; mode sprite one byte to right
;; 2 pixels in mode 0
right_side equ 80-sprite_width

sprite_right:
ld a,(xcoord)
cp right_side
ret z
inc a
ld (xcoord),a
ret

sprite_up:
ld a,(ycoord)
or a
ret z
dec a
ld (ycoord),a
ret


bottom_side equ 199-sprite_height

sprite_down:
ld a,(ycoord)
cp bottom_side
ret z
inc a
ld (ycoord),a
ret


;;---------------------------------------------------------------------
;; firmware function: SCR NEXT BYTE
;;
;; Entry conditions:
;; HL = screen address
;; Exit conditions:
;; HL = updated screen address
;; AF corrupt
;;
;; Assumes:
;; - 16k screen

scr_next_byte:
inc     l
ret     nz

inc     h
ld      a,h
and     &07
ret     nz

;; at this point the address has incremented over a 2048
;; byte boundary.
;;
;; At this point, the next byte on screen is *not* previous byte plus 1.
;;
;; The following is true:
;; 07FF->0000
;; 0FFF->0800
;; 17FF->1000
;; 1FFF->1800
;; 27FF->2000
;; 2FFF->2800
;; 37FF->3000
;; 3FFF->3800
;;
;; The following code adjusts for this case.

ld      a,h
sub     8
ld      h,a
ret     


;;---------------------------------------------------------------------
;; firmware function: SCR NEXT LINE
;;
;; Entry conditions:
;; HL = screen address
;; Exit conditions:
;; HL = updated screen address
;; AF corrupt
;;
;; Assumes:
;; - 16k screen
;; - 80 bytes per line (40 CRTC characters per line)

scr_next_line:
ld      a,h
add     a,8
ld      h,a


and     &38
ret     nz

;; 

ld      a,h
sub     &40
ld      h,a
ld      a,l
add     a,80			;; number of bytes per line
ld      l,a
ret     nc

inc     h
ld      a,h
and     &7
ret     nz

ld      a,h
sub     8
ld      h,a
ret     

;;---------------------------------------------------------------------------------------------------------
;; From our x,y coordinates generate
;; a screen address for top-left of sprite to be drawn at.

;; IN:
;; H = x byte coord
;; L = y line
;; OUT:
;; HL = screen address
get_scr_addr:
push bc
push de

ld b,h

ld a,l
and &7    ;; offset into char
add a,a   ;; x2
add a,a   ;; x4
add a,a   ;; x8 (&0000, &0800, &1000,&1800 etc)
ld c,a      

ld a,(scroll_base)   ;; add on base
add a,a
add a,a
or c
ld c,a    ;; remember

ld a,l
srl a   ;; /2
srl a  ;; /4
srl a ;; /8
;; A = char line down screen

;; now do multiply
ld hl,0     ;; initial value
or a
jr z,gsa2
ld de,40  ;; width in chars (R1)
gsa1: add hl,de
dec a
jr nz,gsa1
gsa2:
ld de,(scroll_offset)    ;; scroll offset
add hl,de     ;; add once
add hl,hl   ;; double offset to get byte offset
ld a,l
add a,b
ld l,a
ld a,h
adc a,0
ld h,a

ld a,h
and &7
or c
ld h,a

pop de
pop bc
ret


;; draw a rectangle onto the screen
;;
;; H = x byte coord
;; L = y line
;; B = height
;; C = width
;;
sprite_draw:
call get_scr_addr

sprite_draw_height:
push bc
push hl
sprite_draw_width:
ld (hl),&cc  ;; fill rectangle with pen 15
call scr_next_byte
dec c
jr nz,sprite_draw_width
pop hl
call scr_next_line
pop bc
djnz sprite_draw_height
ret


;; H = x byte coord
;; L = y line
;; B = height
;; C = width
;;
;; restore a rectangle to the screen
sprite_background_restore:
call get_scr_addr

;; HL = screen address to copy to

;; calculate address in back buffer based on this
;; we can do this because the back buffer is a 1:1 copy of the screen
;; in the same format and everything
ld e,l
ld a,h
and &3f
or &40
ld d,a

sprite_reback_height:
push bc
push hl
push de

sprite_reback_width:
ld a,(de)					;; read from buffer
ld (hl),a					;; write to screen
inc hl
inc de
dec c
jr nz,sprite_reback_width

pop hl
;; back buffer is in same format as screen
;; so we can use the same method to get to the next line
call scr_next_line
ex de,hl

pop hl
call scr_next_line
pop bc
djnz sprite_reback_height
ret

;; table/array for screen addresses for each scan line
scr_addr_table:
defs 200*2


sprite_coords:
;; sprite y line coordinate
ycoord:
defb 0

;; sprite x byte coordinate
xcoord: 
defb 0

;;--------------------------------------------------------------------------------------------

;; 16 for mode 2
;; 8 for mode 1
;; 4 for mode 0

;; reload value for counter
counter_reload equ 8

;;--------------------------------------------------------------------------------------------
;; the counter

counter:
defb counter_reload

;;--------------------------------------------------------------------------------------------

update_scroll:
;; we have a counter. it counts down from counter_reload  to 0.
;; when it reaches 0, we reload it with counter_reload and update
;; the scroll offset.
;;
;; In mode 1, the standard scroll rate is 8 pixels, there are 2 bytes
;; per crtc char with 4 pixels per byte equalling 8 pixels at normal
;; scroll rate.

ld a,(counter)
dec a
ld (counter),a
ret nz
;; reset counter
ld a,counter_reload
ld (counter),a

ld hl,(scroll_offset)
inc hl
ld (scroll_offset),hl
ret

;;--------------------------------------------------------------------------------------------
;; set scroll parameters in hardware
set_scroll:
ld hl,(scroll_offset)

;; write scroll offset (in CRTC character width units)
ld bc,&bc0c				;; select CRTC register 12
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write

;; combine with scroll base
ld a,(scroll_base)
or h
out (c),a

ld bc,&bc0d				;; select CRTC register 13
out (c),c
ld b,&bd				;; B = I/O address for CRTC register write
out (c),l
ret

;;--------------------------------------------------------------------------------------------


;; high byte of the screen base address
;; &00 -> screen uses &0000-&3fff
;; &10 -> screen uses &4000-&7fff
;; &20 -> screen uses &8000-&bfff
;; &30 -> screen uses &c000-&ffff

scroll_base: 
defb &30

;; the scroll offset in CRTC character units
scroll_offset:
defw 0

end start