;; This code shows:
;; 1. How to get the current drive number
;; 2. How to initialise the disc rom after loading this file (BASIC will disable all roms, effectively going to
;; cassette mode)
;; 3. How to set the current drive back again
;; 4. Using a list of files, and loading each file one-by-one. When we get to the end of the list
;; going back to the first file again. Good for slideshows ;)
;;
;; This code builds with Pasmo assembler.

cas_in_open equ &bc77
cas_in_direct equ &bc83
cas_in_close equ &bc7a
kl_rom_walk equ &bccb
mc_start_program equ &bd16

loader_load_addr equ &9000

org loader_load_addr

start_load:
;; when a binary program is started, all roms are disabled by BASIC

;; get the current drive from AMSDOS's variables
ld hl,(&be7d)
ld a,(hl)
ld (drive+1),a

ld c,&ff					;; disable all roms
ld hl,start					;; execution address for program
jp mc_start_program			;; start it


start:
;; now enable all roms (disc rom and other DOSes included)
call kl_rom_walk

;; set back the drive
drive:
ld a,0
ld hl,(&be7d)
ld (hl),a

;; load each file and then go back to the start again
load_loop:
call get_next_filename
call load_next_file
jp load_loop

load_next_file:

ld hl,(cur_filename)
push hl
;; get length of filename
call get_str_len
pop hl
ld de,&200		;; load address
call load_file

;; here we would so something with file (e.g. display picture on screen)
ret

;; get next filename from list and update
;; cur_filename with the address of it
;; if we get to end of list, reset back to start
get_next_filename:
ld hl,(cur_filename)
call get_str_len
ld a,(hl)
or a
jr nz,gnf1
ld hl,filenames
gnf1:
ld (cur_filename),hl
ret

;; Enter:
;; HL = address of string with 0 after
;; Exit:
;; B = length of string
get_str_len:
ld b,0
gsl1:
ld a,(hl)
inc hl
or a
ret z
inc b
jr gsl1

;; the address of the current filename
cur_filename:
defw filenames

;; list of filenames to load 
;; 0 indicates end of filename
filenames:
db "01.scr",0
db "02.scr",0
db "03.scr",0
db "04.scr",0
db "05.scr",0
db "06.scr",0
db "07.scr",0
db "08.scr",0
db "09.scr",0
db "10.scr",0
db "11.scr",0
db "12.scr",0
db "13.scr",0


;; this 0 indicates end of list
defb 0

;; load a file
;; HL = address of filename
;; B = length
;; DE = load address

load_file:
push de
ld de,&c000			;; address of 2k buffer, this can be any value if the files
					;; that are loaded have a header (e.g. BASIC/BINARY)
call cas_in_open	
pop hl
call cas_in_direct
jp cas_in_close

end start_load